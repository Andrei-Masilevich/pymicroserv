# Library for microservices creation. Python version.#
#
# "WEXT Microserv Library"
#

Provides App class with event loop,
logging, configuring, timers, DB adapters and etc.
(minimum required set to create useful microservices).

There is following structure of wext_microserv folder here:

*common*: wext-common

*config*: libconfig parser and logger, DB adapters configuration presets

*app*: App class and internals

*db_node*: DB adapters

*patterns*: Design patterns


It must work on any Python interpretator from 2.7 to 3.x.

=== Requirements:

* wext-common (1.0)
* pyparsing (2.2.0)
* psutil (5.2.2)
* pika (0.10.0)
* psycopg2 (2.7.1)
* redis (2.10.5)
* certifi (2017.4.17)
* tornado (4.5.1)


