#Lib: not executable
# -*- coding: UTF-8 -*-

schemaTrafficLight_libconfig = '''
    mySM:
    { 
        states = 
        (
            {
                id = "initial";
                
                synonym = "yellowBlink";
                
                events = ("switchOn", "switchOff");
                
                transitions = 
                (
                    {
                        event = "switchOn";
                        target = "green";
                    }
                );
            },
            {
                id = "green";
                
                events = ("tick", "longTick");
                
                transitions = 
                (
                    {
                        event = "longTick";
                        target = "blinkGreen";
                    },
                    {
                        event = "switchOff";
                        target = "yellowBlink";
                    }
                );
            },
            {
                id = "blinkGreen";
                
                events = ("tick", "longTick");
                
                transitions = 
                (
                    {
                        event = "tick";
                        target = "red";
                    },
                    {
                        event = "switchOff";
                        target = "yellowBlink";
                    }
                );
            },
            {
                id = "red";
                
                synonym = "forbidden";
                
                events = ("tick", "longTick");
                
                transitions = 
                (
                    {
                        event = "longTick";
                        target = "yellow";
                    },
                    {
                        event = "switchOff";
                        target = "yellowBlink";
                    }
                );
            },
            {
                id = "yellow";
                
                events = ("tick", "longTick");
                
                transitions = 
                (
                    {
                        event = "tick";
                        target = "green";
                    },
                    {
                        event = "switchOff";
                        target = "yellowBlink";
                    }
                );
            },
            {
                id = "error";
                
                transitions = 
                (
                    {
                        target = "yellowBlink";
                    }
                );
            }
        );
    };  
'''

schemaTrafficLight_PyConfig = [
    {
        'id': 'initial',
        'synonym': 'yellowBlink',
        'events': ['switchOn', 'switchOff'],
        'transitions': 
        [
            {
                'event': 'switchOn',
                'target': 'green'
            }
        ],
    },
    {
        'id': 'green',
        'events': ['tick', 'longTick'],
        'transitions': 
        [ 
            {
                'event': 'longTick',
                'target': 'blinkGreen'
            },
            {
                'event': 'switchOff',
                'target': 'yellowBlink'
            }
        ],
    },
    {
        'id': 'blinkGreen',
        'events': ['tick', 'longTick'],
        'transitions': 
        [ 
            {
                'event': 'tick',
                'target': 'red'
            },
            {
                'event': 'switchOff',
                'target': 'yellowBlink'
            }
        ],
    },
    {
        'id': 'red',
        'synonym': 'forbidden',
        'events': ['tick', 'longTick'],
        'transitions': 
        [ 
            {
                'event': 'longTick',
                'target': 'yellow'
            },
            {
                'event': 'switchOff',
                'target': 'yellowBlink'
            }
        ],
    },
    {
        'id': 'yellow',
        'events': ['tick', 'longTick'],
        'transitions': 
        [ 
            {
                'event': 'tick',
                'target': 'green'
            },
            {
                'event': 'switchOff',
                'target': 'yellowBlink'
            }
        ],
    },
    {
        'id': 'error',
        'transitions': 
        [ 
            {
                'target': 'yellowBlink'
            }
        ],
    },
]