#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AsynchStateMachine
from wext_microserv import Singleton

import inspect
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

from .testSM_AppConfig2 import *

import tornado.ioloop
from tornado.ioloop import PeriodicCallback

class AppTest(object, metaclass = AsynchStateMachine):
    
    sm_schema = schemaApp
    
    def __init__(self, name):
        LDBG("SM = %s", self.sm_isRunning())

        self.sm_registerEnterSlot('initial', self.onInitial)
        self.sm_registerExitSlot('initial', self.onExitInitial)

        self.sm_registerEnterSlot('starting', self.onStarting)
        self.sm_registerEnterSlot('running', self.onRunning)
        self.sm_registerEnterSlot('configuring', self.onConfiguring)
        self.sm_registerEnterSlot('stopping', self.onStopping)
        self.sm_registerExitSlot('starting', self.onExitStarting)
        self.sm_registerExitSlot('running', self.onExitRunning)
        self.sm_registerExitSlot('configuring', self.onExitConfiguring)
        self.sm_registerExitSlot('stopping', self.onExitStopping)

    
    def onInitial(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.stop()

    def onStarting(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onRunning(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onConfiguring(self, v):
        LDBG("* %s('%s')", inspect.stack()[0][3], v)

    def onStopping(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
    def onExitInitial(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
    def onExitStarting(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitRunning(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitConfiguring(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitStopping(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
    
    def isStarting(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'starting'
    
    def isRunning(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'running'
    
    def isConfiguring(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'configuring'
    
    def isStopping(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'stopping'
    
    def _next(self, event:str, *params):
        LMSG("* state = {}, _next = ({}, {})".format(self.sm_currentState().split('.')[0], event, params))
        self.sm_next(event, *params)
        
    def run(self):
        LDBG(inspect.stack()[0][3])
        if not self.sm_isRunning():
            io_loop = tornado.ioloop.IOLoop.current()
            
            def _onTick():
                LMSG(inspect.stack()[0][3])
                
            tm = PeriodicCallback(_onTick, 1 * 1000, io_loop)
            tm.start()
            
            io_loop.add_callback(self.sm_next, 'run')
            n = 2
            io_loop.call_later(n, self.run)
            n += 2            
            io_loop.call_later(n, self.run) #ignored
            n += 2
            io_loop.call_later(n, self.config, None)
            n += 2
            io_loop.call_later(n, self.run)
            n += 2            
            io_loop.call_later(n, self.stop)
            n += 2
            io_loop.call_later(n, self.stop)
            io_loop.start()
        else:
            self._next('run')
    
    def config(self, val):
        LDBG(inspect.stack()[0][3])
        self._next('config', val)

    def stop(self):
        LDBG(inspect.stack()[0][3])
        self._next('stop')
        
def test1():
    
    LDBG("=== TEST pyconfig - driven App State Machine")
    
    app = Singleton.instance(AppTest, name = "myAppStateMachine")

    LMSG("%s", app._PATTERN)
    
    app.run()

    return True
    
def test():

    if not test1():
        return False

    return True