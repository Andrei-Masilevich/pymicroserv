#Lib: not executable
# -*- coding: UTF-8 -*-

import wext_microserv
from wext_common.liblog import LDBG, LMSG, LWRN, LERR
import inspect

from wext_microserv import Observable

class EventsSource(Observable):
    
    def test(self, v: int= 0):
        self.fire("event", v)
        
class EventConsumer(object):
    
    result_v = -1
    
    def onEvent(self, v: int):
        LDBG("%s:%s(%r)", hex(id(self)), inspect.stack()[0][3], v)
        if self.result_v == -1:
            self.result_v = v
        else:
            self.result_v = self.result_v + v 

def test():

    src = EventsSource()
    c1 = EventConsumer()
    c2 = EventConsumer()
    
    src.connect("event", c1, "onEvent")
    src.connect("event", c2, "onEvent")
    src.connect("event", c2, "onEvent")
    
    src.test(1)
    
    if c1.result_v != 1 or c2.result_v != 1:
        return False

    src.disconnect(c1)
    
    src.test(1)
    
    if c1.result_v != 1 or c2.result_v != 2:
        return False
    
    return True