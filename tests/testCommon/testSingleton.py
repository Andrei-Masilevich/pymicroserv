#Lib: not executable
# -*- coding: UTF-8 -*-

import wext_microserv
from wext_common.liblog import LDBG, LMSG, LWRN, LERR
import inspect

from wext_microserv import Singleton

class AppTest(object):
    def __init__(self, val1, val2 = "default"):
        self.val1 = val1
        self.val2 = val2
        
    def __repr__(self):
        return "%s(%s: val1=%d, val2='%s')" % (type(self).__name__, hex(id(self)), self.val1, self.val2)
    
class AppTest1(Singleton, AppTest):
    
    def __init__(self, *args, **kwargs):
        if self._singleton_init():
            super(AppTest1, self).__init__(*args, **kwargs)

class AppTest2(AppTest):
    
    pass

class AppTest3(Singleton, AppTest):
    
    def __init__(self, *args, **kwargs):
        if self._singleton_init():
            super(AppTest3, self).__init__(*args, **kwargs)
            
class AppTest4(AppTest3):
    
    pass

def test1(app):
    
    LDBG("=== TEST singleton 1")
    
    app = AppTest1(12, val2 = "myAppTest")
    LDBG("%s", app)
    if not (hasattr(app, "_PATTERN") and "Singleton" in app._PATTERN):
        return False
    
    app2 = AppTest1(13, val2 = "myAppTest (edit)")
    LDBG("%s", app2)
    if not app2 is app:
        return False

    return True
        
def test2(app):
    
    LDBG("=== TEST singleton 2")
    
    app = Singleton.instance(AppTest2, 12, val2 = "myAppTest")
    LDBG("%s", app)
    if not (hasattr(app, "_PATTERN") and "Singleton" in app._PATTERN):
        return False
    
    app2 = Singleton.instance(AppTest2, 13, val2 = "myAppTest (edit)")
    LDBG("%s", app2)
    if not app2 is app:
        return False
    
    return True

def test3(app):
    
    LDBG("=== TEST singleton 3")
    
    app = Singleton.instance(AppTest2, 12, val2 = "myAppTest")
    LDBG("%s", app)
    if not (hasattr(app, "_PATTERN") and "Singleton" in app._PATTERN):
        return False
    
    app2 = AppTest2(13, val2 = "myAppTest (edit)")
    LDBG("%s", app2)
    if app2 is app:
        return False
    
    return True

def test4(app):
    
    LDBG("=== TEST singleton 4")
    
    app = Singleton.instance(AppTest3, 12, val2 = "myAppTest")
    LDBG("%s", app)
    if not (hasattr(app, "_PATTERN") and "Singleton" in app._PATTERN):
        return False
    
    app2 = AppTest3(13, val2 = "myAppTest (edit)")
    LDBG("%s", app2)
    if app2 is app:
        return False
    
    return True

def test():

    app1 = None
    if not test1(app1):
        return False

    app2 = None
    if not test2(app2):
        return False
    
    app3 = None
    if not test3(app3):
        return False
    
    app4 = None
    if not test4(app4):
        return False
    
    return True