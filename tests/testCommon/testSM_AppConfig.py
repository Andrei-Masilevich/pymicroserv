#Lib: not executable
# -*- coding: UTF-8 -*-

schemaApp = {
    "myAppStateMachine": [
        {
            'id': 'initial',
            'synonym': 'stopped',
            'events': ['stop'], #ignored
            'transitions': 
            [
                {
                    'event': 'run',
                    'target': 'starting'
                }
            ],
        },
        {
            'id': 'starting',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'run',
                    'target': 'running'
                },
                {
                    'event': 'stop',
                    'target': 'stopping'
                }
            ],
        },
        {
            'id': 'running',
            'events': ['run'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'stop',
                    'target': 'stopping'
                },
                {
                    'event': 'config',
                    'target': 'configuring'
                }
            ],
        },        
        {
            'id': 'configuring',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'run',
                    'target': 'running'
                },
                {
                    'event': 'stop',
                    'target': 'stopping'
                }
            ],
        },
        {
            'id': 'stopping',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'stop',
                    'target': 'stopped'
                }
            ],
        },
        {
            'id': 'error',
            'transitions': 
            [ 
                {
                    'target': 'stopped'
                }
            ],
        },
    ]}
