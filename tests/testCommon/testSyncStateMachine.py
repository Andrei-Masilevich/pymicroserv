#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import SynchStateMachine

import inspect
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

from .testSM_TLConfig import *
from .testSM_AppConfig import *

class TrafficLight(object, metaclass = SynchStateMachine):
    
    def __init__(self, name, sm_schema):
        self._tickN = 0
    
    def run(self):
        LDBG(inspect.stack()[0][3])
        self.sm_run("switchOn")
        
    def tick(self):
        LDBG(inspect.stack()[0][3])
        if self._tickN % 4 == 0:
            self.sm_next("longTick")
        else:
            self.sm_next("tick")
        self._tickN += 1
            

def test1():
    
    LDBG("=== TEST lconfig - driven Traffic Light State Machine")
    
    tlt = TrafficLight(name = "mySM", sm_schema = schemaTrafficLight_libconfig)
    
    tlt.run()
    
    for ci in range(1, 22):
        tlt.tick()
    
    return True

def test2():

    LDBG("=== TEST pyconfig - driven Traffic Light State Machine")

    tlt = TrafficLight(name = "mySM", sm_schema = schemaTrafficLight_PyConfig)
    
    tlt.run()
    
    for ci in range(1, 22):
        tlt.tick()
    
    return True

class AppTest(object, metaclass = SynchStateMachine):
    
    def __init__(self, name, sm_schema):
        LDBG("SM = %s", self.sm_isRunning())

        self.sm_registerEnterSlot('initial', self.onInitial)
        self.sm_registerExitSlot('initial', self.onExitInitial)

        self.sm_registerEnterSlot('starting', self.onStarting)
        self.sm_registerEnterSlot('running', self.onRunning)
        self.sm_registerEnterSlot('configuring', self.onConfiguring)
        self.sm_registerEnterSlot('stopping', self.onStopping)
        self.sm_registerExitSlot('starting', self.onExitStarting)
        self.sm_registerExitSlot('running', self.onExitRunning)
        self.sm_registerExitSlot('configuring', self.onExitConfiguring)
        self.sm_registerExitSlot('stopping', self.onExitStopping)

    
    def onInitial(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onStarting(self, v):
        LDBG("* %s('%s')", inspect.stack()[0][3], v)

    def onRunning(self, v):
        LDBG("* %s('%s')", inspect.stack()[0][3], v)

    def onConfiguring(self, v):
        LDBG("* %s('%s')", inspect.stack()[0][3], v)

    def onStopping(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
    def onExitInitial(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
    def onExitStarting(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitRunning(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitConfiguring(self):
        LDBG("* %s(None)", inspect.stack()[0][3])

    def onExitStopping(self):
        LDBG("* %s(None)", inspect.stack()[0][3])
        
    def run(self, val):
        LDBG(inspect.stack()[0][3])
        self.sm_run('run', val)
        
    def config(self, val):
        LDBG(inspect.stack()[0][3])
        self.sm_next('config', val)

    def stop(self):
        LDBG(inspect.stack()[0][3])
        self.sm_next('stop')
        
def test3():
    
    LDBG("=== TEST pyconfig - driven App State Machine")
    
    app = AppTest(name = "myAppStateMachine", sm_schema = schemaApp)

    app.run("Run 1")
    app.run("Run 2")
    app.run("Run 3")
    
    app.config("Config 1")
    app.config("Config 2")
    app.config("Config 3")

    app.stop()
    app.stop()
    app.stop()
    
    return True
    
def test():

    if not test1():
        return False
    
    if not test2():
        return False
    
    if not test3():
        return False

    return True