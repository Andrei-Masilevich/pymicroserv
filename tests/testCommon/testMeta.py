#Lib: not executable
# -*- coding: UTF-8 -*-

import wext_microserv
from wext_common.liblog import LDBG, LMSG, LWRN, LERR
import inspect

'''
Base class substitution technique
'''
class SubstitutionMeta(type):
    
    class _A0(object):
        def __init__(self):
            super(SubstitutionMeta._A0, self).__init__()
            LDBG("-- %s.%s", "SubstitutionMeta._A0", inspect.stack()[0][3])
    
        def meth(self):
            LDBG("%s.%s", "SubstitutionMeta._A0", inspect.stack()[0][3])
            return -1

    def __new__(self, name, bases, dct):
        LDBG("meta: __new__ %s %s %s", name, bases, list(dct))
        
        newBase = [self._A0]
        newBase.extend(list(bases))
        dct["_PATTERN"] = self.__name__
        return type.__new__(self, name, tuple(newBase), dct)

    def __call__(self, *args, **kwargs):
        LDBG("meta: __call__ %s %s", list(args), list(kwargs))
        
        return super(SubstitutionMeta, self).__call__(*args, **kwargs)

class A(object):
    def __init__(self):
        self.v = 0
        super(A, self).__init__()
        LDBG("-- %s.%s", "A", inspect.stack()[0][3])

    def meth(self):
        LDBG("%s.%s", "A", inspect.stack()[0][3])
        return self.v

class B(object):
    def __init__(self):
        self.v = 0
        super(B, self).__init__()
        LDBG("-- %s.%s", "B", inspect.stack()[0][3])

    def meth(self):
        LDBG("%s.%s", "B", inspect.stack()[0][3])
        return self.v

class C(object, metaclass = SubstitutionMeta):
    def __init__(self, v: int):
        self.v = v
        super(C, self).__init__()
        LDBG("-- %s.%s", "C", inspect.stack()[0][3])

    def meth(self):
        LDBG("%s.%s", "C", inspect.stack()[0][3])
        return self.v
        
class D(A, B, metaclass = SubstitutionMeta):
    def __init__(self, v: int):
        self.v = v
        super(D, self).__init__()
        LDBG("-- %s.%s", "D", inspect.stack()[0][3])

def test():
    c_obj = C(10)
    if c_obj.meth() != 10:
        return False
    
    c_obj = D(12)
    if c_obj.meth() != -1:
        return False
    return True