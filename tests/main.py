#!/usr/bin/env python2

def main(args):

    import sys
    from os.path import dirname, join, normpath
    sys.path.append(join(dirname(__file__), "..")) #to find wext_microserv

    from os import environ as e
    import os.path as p
    from wext_microserv import App
    from testApp import AppTestImpl

    if not e.get("WINGDB_ACTIVE"):
        e["PYTHONOPTIMIZE"]="1"
    
    rootPath = p.dirname(__file__)
    app = App(AppTestImpl,
              name = e.get("APP_NAME"), 
              description = "Test only mocroservice to test WEXT-microserv Lib", 
              rootPath = rootPath, 
              defaultConfigPath = p.join(rootPath, "conf", "default.conf"))
    return app.run(args)
    
if __name__=="__main__":

    import sys
    
    ret = main(sys.argv[1:])
    sys.exit(ret)
