#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AppImpl, App
import inspect
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

class AppSimpleTest(AppImpl):
    
    def onConfigInit(self, app, args, confObj):
        LDBG(inspect.stack()[0][3])
        
        if self.time0:
            App.app.addTimer(self.time0*1000, App.app.stop, repeat = False)
        
        if self.time1:
            App.app.addTimer(self.time1*1000, self._onPayloadRun, repeat = True)
    
    def onDaemonInit(self, app, args):
        LDBG(inspect.stack()[0][3])
        return () #*daemon_params
        
    def onRun(self, app, args):
        LDBG(inspect.stack()[0][3])
    
    def _onPayloadRun(self):
        txt = "Do payload"
        LDBG(txt)
        self.log.debug(txt)