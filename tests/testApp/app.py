#Lib: not executable
# -*- coding: UTF-8 -*-

from os import environ as e

if not e.get("APP_NAME"):
    e["APP_NAME"]="pyMicroservice" #used in headers!

from wext_microserv import AppImpl

import logging
import inspect

from .appSimple import AppSimpleTest
from .appTestNode import AppTestNode
from .appTestRabbit import AppTestRabbit
from .appTestRedis import AppTestRedis
from .appTestPostgres import AppTestPostgres

from tornado import gen

from wext_common.liblog import LDBG, LMSG, LWRN, LERR

class AppTestImpl(AppImpl):
    
    APP_IMPLS = \
        [
            ("app", 
             "Simple application test",
             AppSimpleTest),
            
            ("node", 
             "Test node (base class check)",
             AppTestNode),
            
            ("rabbit", 
             "Test for blocked RabbitMQ node",
             AppTestRabbit),
        
            ("redis", 
             "Test for Redis node",
             AppTestRedis),
        
            ("postres", 
             "Test for PostreSQL DB node",
             AppTestPostgres)
        ]
    
    def __init__(self):
        self.appImpl = None
        self.log = None
        
    N_TIMERS = 10
    
    def onArgumentsInit(self, app, parser):
        from argparse import ArgumentParser
        
        assert isinstance(parser, ArgumentParser), "Invalid argument type recieved"
        
        subs = parser.add_subparsers()

        for w in self.APP_IMPLS:
            (name, help, cls) = w
            
            sp = subs.add_parser(name, help=help)
            sp.set_defaults(mode=name)
            sp.add_argument( "-l", "--time0", 
                             type = int, 
                             default = 60, 
                             help="Time to live in seconds" )
            for ci in range(1, self.N_TIMERS):
                sp.add_argument( "-t%d" % ci, 
                                 "--time%d" % ci, 
                                 type = int, 
                                 default = 0, 
                                 help="Timer %d" % ci )
        
    def onConfigInit(self, app, args, confObj):
        #create loggers
        self.log = logging.getLogger(app.name())
            
        self.log.debug(inspect.stack()[0][3])
        
        if not self.appImpl:
            
            for w in self.APP_IMPLS:
                (name, help, cls) = w
         
                if args.mode == name:
                    
                    params = {}
                    for ci in range(0, self.N_TIMERS):
                        param_key = "time%d" % ci
                        param_val = getattr(args, param_key, None)
                        if param_val:
                            params[param_key] = param_val
                    self.appImpl = cls()
                    setattr(self.appImpl, "parent", self)
                    setattr(self.appImpl, "app", app)
                    _log = logging.getLogger(type(self.appImpl).__name__.lower())
                    setattr(self.appImpl, "log", _log)
                    for ci in range(0, AppTestImpl.N_TIMERS):
                        param_key = "time%d" % ci
                        param_val = params.get(param_key)
                        setattr(self.appImpl, param_key, param_val)
                    
                    self.log.info("Test '%s' applied", name)
                    break
        
        assert self.appImpl, "Test App not found"
        
        #configurate business logic (and DB adapters)
        self.appImpl.onConfigInit(app, args, confObj)
    
    def onDaemonInit(self, app, args):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.appImpl, "Test App not found"
        
        return self.appImpl.onDaemonInit(app, args)

    @gen.coroutine
    def onStart(self, app, args, *daemon_params):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.appImpl, "Test App not found"

        result = yield self.appImpl.onStart(app, args, *daemon_params)
        raise gen.Return(result)
    
    @gen.coroutine
    def onRun(self, app, args):
        self.log.debug(inspect.stack()[0][3])

        assert self.appImpl, "Test App not found"

        self.appImpl.onRun(app, args)
        raise gen.Return(True)

    @gen.coroutine
    def onConfigReload(self, app, args, confObj):
        assert self.appImpl, "Test App not found"
            
        #recreate loggers
        self.log = logging.getLogger(app.name())
            
        self.log.debug(inspect.stack()[0][3])
        
        #configurate business logic (and DB adapters)
        result = yield self.appImpl.onConfigReload(app, args, confObj)
        raise gen.Return(result)
        
    @gen.coroutine
    def onStop(self, app, args):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.appImpl, "Test App not found"
        
        result = yield self.appImpl.onStop(app, args)
        raise gen.Return(result)