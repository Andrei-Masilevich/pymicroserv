#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AppImpl
from wext_microserv import RedisNode

import logging
import inspect

from wext_common.libtrace import SLEEP
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

class AppTestRedis(AppImpl):
    
    def __init__(self):
        self.rnode = None
    
    def onConfigInit(self, app, args, confObj):
        
        if not self.rnode:
            self.rnode = RedisNode(conf = confObj.get("redis"), name = "DEV", master_mode = True)
            

    def onStart(self, app, args, *daemon_params):
            
        assert self.rnode, "Unexpected for this test"
        
        if self.time1:
            app.addTimer(self.time1*1000, self._onInsert, repeat = True)
        if self.time2:    
            app.addTimer(self.time2*1000, self._onGet, repeat = True)
        if self.time3:            
            app.addTimer(self.time3*1000, self._onDelete, repeat = True)
        if not self.time4:
            self.time4 = self.rnode.PING_TIMEOUT_MS
        app.addTimer(self.time4, self._onPing, repeat = True)         
        
        if self.time0:
            app.addTimer(self.time0*1000, app.stop, repeat = False)

        if self.rnode.connect():
            if self.rnode.connection().client_getname():
                addr = None
                for cli in self.rnode.connection().client_list():
                    name = cli["name"]                    
                    if self.rnode.connection().client_getname() == name:
                        addr = cli["addr"] 
                        break;
                self.log.info("Connection '%s' from '%s' to Redis established", str(name), str(addr))
            else:
                self.log.info("Connection to Redis established")
        else:
            app.stop()
    
    def onStop(self, app, args):
        if self.rnode:
            self.rnode.disconnect()
    
    N = 9000
    KEY_MASK = "smscampaign:task"
    
    def _onInsert(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
    
        self.N += 1

        key = "%s:%d" % (self.KEY_MASK, self.N)
        
        import redis
        
        data = (key, { "name": "Piter Pan", "status": 1 })
        try:
            pipe = self.rnode.connection().pipeline()
            
            pipe.hmset(data[0], data[1]).expire(data[0], 60).execute()
            self.log.debug("Save %s -> '%s'", data[0], str(data[1]))
            
        except redis.exceptions.ReadOnlyError as err:
            self.log.error("Can't paste into readonly Redis. Error '%s'", err)
        except Exception as err:
            self.log.error('Failed to insert, try next time. Error: "%s"', err)
        
    def _onGet(self):
        
        self.log.debug(inspect.stack()[0][3])

        assert self.rnode, "Unexpected for this test"
        
        import redis
        
        try:
            r = self.rnode.connection()
            
            mask = "%s:*" % (self.KEY_MASK)
            
            for key in r.scan_iter(mask):
                self.log.debug("Load %s -> '%s'", key, str(r.hgetall(key)))
                
        except redis.exceptions.ReadOnlyError as err:
            self.log.error("Can't paste into readonly Redis. Error '%s'", err)
        except Exception as err:
            self.log.error('Failed to insert, try next time. Error: "%s"', err) 
    
    def _onDelete(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        import redis
        
        try:
            r = self.rnode.connection()
            
            mask = "%s:*" % (self.KEY_MASK)
            
            for key in r.scan_iter(mask):
                self.log.debug("Delete %s -> '%s'", key, str(r.hgetall(key)))
                r.delete(key)
                break
            
        except redis.exceptions.ReadOnlyError as err:
            self.log.error("Can't paste into readonly Redis. Error '%s'", err)
        except Exception as err:
            self.log.error('Failed to insert, try next time. Error: "%s"', err)         
     
    def _onPing(self):

        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        self.rnode.ping()
