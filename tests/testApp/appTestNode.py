#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AppImpl, App
from .test_node import TestNode

import logging
import inspect

from wext_common.libtrace import SLEEP
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

from tornado import gen

class AppTestNode(AppImpl):
    
    def __init__(self):
        self.rnode1 = None
        self.rnode2 = None
        self.timers = []
    
    def onConfigInit(self, app, args, confObj):
        self.log.debug(inspect.stack()[0][3])
        
        if not self.rnode1:
            self.rnode1 = TestNode(conf = confObj.get("node"), name = "NODE_Foo1")
        if not self.rnode2:
            self.rnode2 = TestNode(conf = confObj.get("node"), name = "NODE_Foo2")

    @gen.coroutine
    def onStart(self, app, args, *daemon_params):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode1 and self.rnode2, "Unexpected for this test"
        
        result = yield self.rnode1.connect()
        if result:
            self.log.info("Connection to Foo1 established")
        result = yield self.rnode2.connect()
        result = yield self.rnode2.connect() #must be ignored
        result = yield self.rnode2.connect()
        if result:
            self.log.info("Connection to Foo2 established")
        
        if result:
            if self.time1:
                self.timers.append(app.addTimer(self.time1*1000, self._onTest, repeat = True))
            if self.time2:
                self.timers.append(app.addTimer(self.time2*1000, self._onPing, repeat = True))
            
            if self.time0:
                app.addTimer(self.time0*1000, app.stop)
            if self.time3:
                app.addTimer(self.time3*1000, app.configure)
            
        raise gen.Return(result)
    
    @gen.coroutine
    def onConfigReload(self, app, args, confObj):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode1 and self.rnode2, "Unexpected for this test"
        
        result = yield self.onStop(app, args)
        if not result:
            raise gen.Return(result)
        
        del self.rnode1
        self.rnode1 = TestNode(conf = confObj.get("node"), name = "NODE_Foo1")
        del self.rnode2
        self.rnode2 = TestNode(conf = confObj.get("node"), name = "NODE_Foo2")
            
        result = yield self.rnode1.connect()
        if not result:
            raise gen.Return(result)        
        self.log.info("Connection to Foo1 established")
        result = yield self.rnode2.connect()
        if not result:
            raise gen.Return(result)        
        self.log.info("Connection to Foo2 established")        
        
        if self.time1:
            self.timers.append(app.addTimer(self.time1*1000, self._onTest, repeat = True))
        if self.time2:
            self.timers.append(app.addTimer(self.time2*1000, self._onPing, repeat = True))
            
        raise gen.Return(True)     

    def onRun(self, app, args):
        self.log.debug(inspect.stack()[0][3])
        
        pass
    
    @gen.coroutine
    def onStop(self, app, args):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode1 and self.rnode2, "Unexpected for this test"

        for tm in self.timers:
            app.removeTimer(tm)
        self.timers = []
            
        result = yield self.rnode1.disconnect()
        if not result:
            raise gen.Return(result)          
        self.log.info("Connection to Foo1 closed")
        result = yield self.rnode2.disconnect()
        if not result:
            raise gen.Return(result)          
        self.log.info("Connection to Foo2 closed")
        
        raise gen.Return(True)  
        
    def _onTest(self):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode1, "Unexpected for this test"
    
        self.rnode1.doSmthWithConnection()

    def _onPing(self):
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode1, "Unexpected for this test"
        
        self.rnode1.ping()


