#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AppImpl
from wext_microserv import PostgresNode

import logging
import inspect

from wext_common.libtrace import SLEEP
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

class AppTestPostgres(AppImpl):
    
    def __init__(self):
        self.rnode = None
    
    def onConfigInit(self, app, args, confObj):
           
        if not self.rnode:
            self.rnode = PostgresNode(conf = confObj.get("db"), name = "DEV")
            

    def onStart(self, app, args, *daemon_params):
        
        assert self.rnode, "Unexpected for this test"
        
        if self.time1:
            app.addTimer(self.time1*1000, self._onGet, repeat = True)
        if self.time2:    
            app.addTimer(self.time2*1000, self._onCall, repeat = True)
        if self.time3:            
            app.addTimer(self.time3*1000, self._onInsert, repeat = True)
        if self.time4:  
            app.addTimer(self.time4*1000, self._onUpdate, repeat = True)
        if not self.time5:
            self.time5 = self.rnode.PING_TIMEOUT_MS
        app.addTimer(self.time5, self._onPing, repeat = True) 
        
        if self.time0:
            app.addTimer(self.time0*1000, app.stop, repeat = False)
        
        if self.rnode.connect():
            self.log.info("Connection to Postgres established")
        else:
            app.stop()
    
    def onStop(self, app, args):
        if self.rnode:
            self.rnode.disconnect()
    
    def _onGet(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        for row in self.rnode.get(
            "select * from point_servers p where p.fipoint_server_soap_port > %s", 
            [3000, ]):
            self.log.info(row)
                
    def _onCall(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        for row in self.rnode.callproc(
            "sw_iwebapicov.get_sms_cmpn_tasks", 
            [1, 100]):
            self.log.info(row)  
                
    def _onInsert(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        #TODO                                
                
    def _onUpdate(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        #TODO                

    def _onPing(self):

        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnode, "Unexpected for this test"
        
        self.rnode.ping()
        