from wext_microserv import App
from wext_microserv import DBNode

from tornado import gen
import tornado.ioloop
import inspect
    
class ExternalFooAsynchLib(object):
    
    def __init__(self, log):
        self.log = log
    
    def connectionStep1(self, arg1, callback):
        self.log.debug("%s(%r)", inspect.stack()[0][3], arg1)
    
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.call_later(1, callback, "First step of connection", arg1)
        
    def connectionStep2(self, arg1, arg2, callback):
        self.log.debug("%s(%r, %r)", inspect.stack()[0][3], arg1, arg2)

        if 999 == arg2:
            raise ValueError("Test error")
    
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.call_later(1, callback, "Second step of connection", arg1, arg2)

    def connectionStep3(self, callback):
        self.log.debug("%s()", inspect.stack()[0][3])
    
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.call_later(1, callback, "My Foo connection")
        
    def disconnect(self, callback):
        self.log.debug("%s()", inspect.stack()[0][3])

        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.call_later(1, callback)
    
    def ping(self, callback):
        self.log.debug("%s()", inspect.stack()[0][3])

        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.call_later(0.5, callback)

    def doSmthWithConnection(self, conn, raiseError = False):
        if raiseError:
            raise ValueError("Test error")
        self.log.debug("%r <-> Bla Bla Bla", conn)
        
class TestNode(DBNode):
    
    def __init__(self, conf, name):
        
        super(TestNode, self).__init__(conf, name)
        
        self.foo = ExternalFooAsynchLib(self.log)

    @gen.coroutine
    def _createConnection(self, **callbacks):
        self.log.debug("%s(%r)", inspect.stack()[0][3], callbacks)
        
        io_loop = tornado.ioloop.IOLoop.current()
        ret = yield gen.Task(io_loop.add_timeout, io_loop.time() + 1)
        self.log.debug("%s -> no step -> ret = %r", inspect.stack()[0][3], ret)
            
        ret = yield gen.Task(self.foo.connectionStep1, 12)
        self.log.debug("%s - connectionStep1 -> ret = %r", inspect.stack()[0][3], ret)
        
        ret = yield gen.Task(self.foo.connectionStep2, 11, 12)
        self.log.debug("%s - connectionStep2 -> ret = %r", inspect.stack()[0][3], ret)

        ret = yield gen.Task(self.foo.connectionStep3)
        self.log.debug("%s connectionStep3 -> ret = %r", inspect.stack()[0][3], ret)
        
        raise gen.Return(ret)
    
    @gen.coroutine
    def _closeConnection(self, conn):
        self.log.debug("%s(%r)", inspect.stack()[0][3], conn)
        
        yield gen.Task(self.foo.disconnect)
        self.log.debug("%s -> disconnect", inspect.stack()[0][3])
        
        raise gen.Return(True)

    @gen.coroutine
    def _pingConnection(self, conn):
        self.log.debug("%s(%r)", inspect.stack()[0][3], conn)
    
        yield gen.Task(self.foo.ping)
        self.log.debug("%s -> ping", inspect.stack()[0][3])
        
        raise gen.Return(True)
    
    @gen.coroutine
    def doSmthWithConnection(self):
        yield self.invalidateConnection(self.foo.doSmthWithConnection, self.connection())