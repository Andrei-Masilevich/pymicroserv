#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_microserv import AppImpl
from wext_microserv import RabbitMQNode

import logging
import inspect

from wext_common.libtrace import SLEEP
from wext_common.liblog import LDBG, LMSG, LWRN, LERR

from tornado import gen

class AppTestRabbit(AppImpl):
    
    def __init__(self):
        self.rnodeProducer = None
        self.rnodeConsumer = None
        self.timers = []
    
    def onConfigInit(self, app, args, confObj):
        
        self.rnodeProducer = RabbitMQNode(conf = confObj.get("rabbitmq"), name = "producer")
        self.rnodeConsumer = RabbitMQNode(conf = confObj.get("rabbitmq"), name = "consumer")

    @gen.coroutine
    def onStart(self, app, args, *daemon_params):
        self.log.debug(inspect.stack()[0][3])
    
        assert self.rnodeProducer, "Unexpected for this test"
        assert self.rnodeConsumer, "Unexpected for this test"
        
        result = yield self.rnodeProducer.connect()
        if not result:
            raise gen.Return(False)

        self.log.info("Connection to Rabbit Producer established")
        
        result = yield self.rnodeConsumer.connect(onMessageCallback = self._onMessage)
        if not result:
            raise gen.Return(False)
            
        self.log.info("Connection to Rabbit Consumer established")
        
        if self.time1:    
            self.timers.append(app.addTimer(self.time1*1000, self._onSend, repeat = True))
            
        if self.time0: 
            app.addTimer(self.time0*1000, app.stop)
            
        if self.time2:
            app.addTimer(self.time2*1000, app.configure)
                
        raise gen.Return(result)
        
    @gen.coroutine
    def onStop(self, app, args):
        self.log.debug(inspect.stack()[0][3])
    
        assert self.rnodeProducer, "Unexpected for this test"
        assert self.rnodeConsumer, "Unexpected for this test"

        for tm in self.timers:
            app.removeTimer(tm)
        self.timers = []

        result1 = yield self.rnodeProducer.disconnect()
        if result1:
            self.log.info("Connection to Rabbit Producer closed")
            
        result2 = yield self.rnodeConsumer.disconnect()
        if result2:
            self.log.info("Connection to Rabbit Consumer closed")

        raise gen.Return(result1 and result2)
    
    @gen.coroutine
    def onConfigReload(self, app, args, confObj):
        self.log.debug(inspect.stack()[0][3])
        
        result = yield self.onStop(app, args)
        if not result:
            raise gen.Return(result)
        
        del self.rnodeProducer
        self.rnodeProducer = RabbitMQNode(conf = confObj.get("rabbitmq"), name = "producer")
        del self.rnodeConsumer
        self.rnodeConsumer = RabbitMQNode(conf = confObj.get("rabbitmq"), name = "consumer")
            
        result = yield self.rnodeProducer.connect()
        if not result:
            raise gen.Return(result)

        self.log.info("Connection to Rabbit Producer established")

        result = yield self.rnodeConsumer.connect(onMessageCallback = self._onMessage)
        if not result:
            raise gen.Return(result)
        
        self.log.info("Connection to Rabbit Consumer established")
        
        if self.time1:    
            self.timers.append(app.addTimer(self.time1*1000, self._onSend, repeat = True))
        
        raise gen.Return(True)
    
    N = 0
    
    def _onSend(self):
        
        self.log.debug(inspect.stack()[0][3])
        
        assert self.rnodeProducer, "Unexpected for this test"
        
        import uuid

        for ntf in ['notify1', 'notify2']:
            self.N += 1
            
            routing_key = self.rnodeProducer.ROUTING_KEY
            routing_key = routing_key.replace('#', ntf)
            
            data = "[%d, '%s']-'%s'" % (self.N, ntf, uuid.uuid4())
            self.rnodeProducer.publishMessage(data, routing_key = routing_key)
        
    def _onMessage(self, **data):

        self.log.debug(inspect.stack()[0][3])

        self.log.info("routing_key = %r, body =  %r", data.get('routing_key'), data.get('body'))
