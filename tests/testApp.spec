# -*- mode: python -*-

block_cipher = None

main_a = Analysis(['../main.py'],
             pathex=['.', 
             './common/wext_common', 
             './common/wext_config'],
             binaries=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
main_pyz = PYZ(main_a.pure, main_a.zipped_data,
             cipher=block_cipher)
main_exe = EXE(main_pyz,
          main_a.scripts,
          main_a.binaries,
          main_a.zipfiles,
          main_a.datas,
          name='pySmsOutbound',
          debug=False,
          strip=True,
          upx=True,
          console=True )
