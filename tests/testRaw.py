#!/usr/bin/env python2

if __name__=="__main__":

    from os import environ as e
    from os.path import dirname, join, normpath
    
    if not e.get("DEBUG"):
        e["DEBUG"]="True"
    if not e.get("APP_NAME"):
        e["APP_NAME"]="pyMicroservice"
    #e["WEXT_COMMON_DEBUG"]='1'
        
    import sys
    from main import main

    ret = main("-c ~/_Conf/pymicroserv.conf "
               "--daemon -p ~/_Pids/pymicroserv.pid app -l 60 -t1 2".split())
    sys.exit(ret)
