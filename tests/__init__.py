# -*- coding: UTF-8 -*-
        
from unittest import TestCase, main as start_tests
import sys
from os import environ as e
from os.path import dirname, join, normpath

if e.get("WINGDB_ACTIVE"):
    if not e.get("DEBUG"):
        e["DEBUG"]="True"
    if not e.get("APP_NAME"):
        e["APP_NAME"]="pyMicroservice"
    e["WEXT_COMMON_DEBUG"]='1'

_init = False


# noinspection PyPackageRequirements
class tests(TestCase):
    def setUp(self):
        global _init
        if not _init:  #any objects for each test*
            _init = True
            print(sys.executable)

    def test04(self):
        from testCommon import testSyncStateMachine
        return testSyncStateMachine.test()
    
    def test05(self):
        from testCommon import testAsyncStateMachine
        return testAsyncStateMachine.test()
    
    def test10(self):
        from main import main
        return 0 == main("-c ~/_Conf/pymicroserv.conf app -l 10 -t1 2".split())
    
    '''
    def test01(self):
        from testCommon import testMeta
        return testMeta.test() 
    
    def test02(self):
        from testCommon import testSingleton
        return testSingleton.test() 

    def test03(self):
        from testCommon import testObserver
        return testObserver.test()
    
    def test11(self):
        from main import main
        return 0 == main("-c ~/_Conf/pymicroserv.conf node -l 40 -t1 1 -t2 1 -t3 5".split())
    
    def test12(self):
        from main import main
        return 0 == main("-c ~/_Conf/pymicroserv.conf rabbit -l 40 -t1 3 -t2 20".split())
    ''' 
        
if __name__ == "__main__":
    try:            
        start_tests()
    except Exception as e:
        print(e)
    sys.exit(0)
    