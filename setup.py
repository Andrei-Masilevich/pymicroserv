from setuptools import setup, find_packages

__version__ = 1.0 #TODO: from .mak, deflist

def read_file(filename):
    with open(filename) as f:
        return f.read()

setup(
    name='wext-microserv',
    version=__version__,
    description='WEXT Microserv Library: '
                'Library for microservices creation',
    install_requires=[
        'wext-common>=1.0', #logger, utils
        'psutil==5.4.7', #for daemonizing
        'pyparsing==2.2.0', #language for config files
        'tornado==4.5.3', #async loop, times
        'pika==0.10.0', #RabbitMQ 
        'psycopg2==2.7.1', #PostgreSQL
        'redis==2.10.5',
                ],
    author='Andrei Masilevich',
    author_email='a.masilevich@yandex.ru',
    long_description=read_file('README.md'),
    license=read_file('LICENSE'),
    keywords=[
        'library',
        'microservice',
        'eventloop',
        'timers',
        'server',
        'helpers',
        'libconfig',
        'configuration',
        'logger',
        'rabbit',
        'rabbitmq',
        'amqp',
        'redis',
        'sentinel',
        'postres',
        'postresql',  
        ],
    platforms=[
        'Ubuntu 16 x86 (tested)', 
        'CentOS 6 AMD64', 
        'Windows 7 x64'],
    packages=['wext_microserv'],
    package_dir = {'wext_microserv': 'wext_microserv'}, 
    include_package_data=True,
    entry_points={},
    test_suite='tests',
)
