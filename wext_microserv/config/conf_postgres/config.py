from ..libconfig import ConfGroup

class ConfPostgres(type):
    
    def __call__(self, *args, **kwargs):

        obj = self.__new__(self, *args, **kwargs)
        
        conf = kwargs["conf"]
        if not isinstance(conf, ConfGroup):
            raise TypeError("Config object must be group")

        nodeName = kwargs["name"]
        
        con = conf.get(nodeName)
        if not con or not isinstance(con, ConfGroup):
            raise ValueError("Section not found")
        
        val = con.get("host")
        if not val:
            val = "localhost"
            
        setattr(obj, "HOST", str(val))

        val = con.get("port", 5432)
        if not isinstance(val, int) or not val in range(1024, 65535):
            raise ValueError("Invalid port value")
            
        setattr(obj, "PORT", val)

        val = con.get("dbname", "postgres")
            
        setattr(obj, "DBNAME", str(val))

        val = con.get("user", "postgres")
            
        setattr(obj, "USER", str(val))

        val = con.get("password", "postgres")
            
        setattr(obj, "PSW", str(val))

        val = con.get("min_connections", 1)
        if not isinstance(val, int) or not val in range(1, 10):
            raise ValueError("Invalid min_connections value")
            
        setattr(obj, "MIN_CONNECTIONS", val)
        minV = val
        
        val = con.get("max_connections", 2)
        if not isinstance(val, int) or not val in range(1, 10):
            raise ValueErrorr("Invalid max_connections value")
        
        if minV > val:
            raise ValueError("min_connections > max_connections")
            
        setattr(obj, "MAX_CONNECTIONS", val)
              
        obj.__init__(*args, **kwargs)
        return obj
    
'''
    //
    //------- CONFIG SECTION EXAMPLE
    //
    
    db:
    {
        DEV:
        {
            host = "localhost";
            port = 5432;
            dbname = "postgres";
            user = "postgres";
            password = "postgres";
            max_connections = 2;
        };
    };
    
'''
