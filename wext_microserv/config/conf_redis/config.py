from ..libconfig import ConfGroup, ConfList

class ConfRedis(type):
    
    def __call__(self, *args, **kwargs):

        obj = self.__new__(self, *args, **kwargs)
        
        conf = kwargs["conf"]
        if not isinstance(conf, ConfGroup):
            raise TypeError("Config object must be group")

        nodeName = kwargs["name"]
        
        con = conf.get(nodeName)
        if not con or not isinstance(con, ConfGroup):
            raise ValueError("Section not found")
        
        val = con.get("host")
        if not val:
            val = "localhost"
            
        setattr(obj, "HOST", str(val))

        val = con.get("port", 6379)
        if not isinstance(val, int) or not val in range(1024, 65535):
            raise ValueError("Invalid port value")
            
        setattr(obj, "PORT", val)

        val = con.get("password")
        if not val:
            val = "guest"
            
        setattr(obj, "PSW", str(val))
        
        val = con.get("dbnum", 0)
        if not isinstance(val, int):
            raise ValueError("Invalid dbnum value")
            
        setattr(obj, "DBNUM", val)

        val = con.get("socket_timeout_ms", 100)
        if not isinstance(val, int) or val < 10:
            raise ValueError("Invalid socket_timeout_ms value")
        
        setattr(obj, "SOCKET_TIMEOUT_MS", val)        

        val = con.get("retry_on_timeout", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid retry_on_timeout value")
            
        setattr(obj, "RETRY_ON_TIMEOUT", val)           

        val = con.get("max_connections", 2)
        if not isinstance(val, int) or val < 1:
            raise ValueError("Invalid max_connections value")
        
        setattr(obj, "MAX_CONNECTIONS", val)        
        
        val = con.get("mark_connection", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid mark_connection value")
            
        setattr(obj, "MARK_CONNECTION", val)    
        
        lSentinels = []
        
        val = con.get("sentinels")
        if val and not isinstance(val, ConfList):
            raise ValueError("Invalid sentinels value")
        
        if val:
            for st in val:
                if not isinstance(st, ConfGroup):
                    raise ValueError("Invalid sentinel section value")
                
                val = st.get("host", "localhost")
                    
                stHost = str(val)
        
                val = st.get("port", 26379)
                if not isinstance(val, int) or not val in range(1024, 65535):
                    raise ValueError("Invalid port value")
                
                stPort = val
                
                lSentinels.append((stHost, stPort))
               
        setattr(obj, "SENTINELS", lSentinels) 
        
        obj.__init__(*args, **kwargs)
        return obj
    
'''
    //
    //------- CONFIG SECTION EXAMPLE
    //
    
    redis:
    {
        DEV:
        {
            host = "192.168.37.133"; // for direct connection without sentinels
            port = 6379;
            password = "ccredis";
            dbnum = 1;
            socket_timeout_ms = 100;
            max_connections = 2;
            sentinels =
            (
                {
                        host = "192.168.37.133";
                        port = 26379;
                },
                {
                        host = "192.168.37.134";
                        port = 26379;
                },
                {
                        host = "192.168.37.135";
                        port = 26379;
                }
            );
        };
    };

    
'''
