from ..libconfig import ConfGroup

class ConfRabbitMQ(type):
    
    def __call__(self, *args, **kwargs):

        obj = self.__new__(self, *args, **kwargs)
        
        conf = kwargs["conf"]
        if not isinstance(conf, ConfGroup):
            raise TypeError("Config object must be group")

        nodeName = kwargs["name"]
        
        v = conf.get("nodes")
        if not v or not isinstance(v, ConfGroup):
            raise ValueError("Section 'nodes' not found")
        node = v.get(nodeName)
        if not node or not isinstance(v, ConfGroup):
            raise ValueError("Section not found")
        
        conName = node.get("connection")
        if not conName:
            raise ValueError("Section not found")
        
        v = conf.get("connections")
        if not v or not isinstance(v, ConfGroup):
            raise ValueError("Section 'connections' not found")
        con = v.get(conName)
        if not con or not isinstance(v, ConfGroup):
            raise ValueError("Section not found")
        
        val = con.get("host")
        if not val:
            val = "localhost"
            
        setattr(obj, "HOST", str(val))

        val = con.get("port", 5672)
        if not isinstance(val, int) or not val in range(1024, 65535):
            raise ValueError("Invalid port value")
            
        setattr(obj, "PORT", val)

        val = con.get("vhost")
        if not val:
            val = "/"
            
        setattr(obj, "VHOST", str(val))

        val = con.get("login")
        if not val:
            val = "guest"
            
        setattr(obj, "LOGIN", str(val))

        val = con.get("password")
        if not val:
            val = "guest"
            
        setattr(obj, "PSW", str(val))

        val = con.get("frame_max", 131072)
        if not isinstance(val, int) or val < 1:
            raise ValueError("Invalid frame_max value")
            
        setattr(obj, "FRAME_MAX", val)

        val = con.get("heartbeat_s", 8)
        if not isinstance(val, int) or val < 1:
            raise ValueError("Invalid heartbeat_s value")
            
        setattr(obj, "HEARTBEAT_S", val)
        
        val = node.get("exchange_name")
        if not val:
            raise ValueError("Exchange name (exchange_name) required!")
            
        setattr(obj, "EXCHANGE_NAME", str(val))

        val = node.get("exchange_type", "topic")
        if not isinstance(val, str) or not val in ["direct", "topic", "headers", "fanout"]:
            raise ValueError("Invalid exchange_type value")
            
        setattr(obj, "EXCHANGE_TYPE", val)

        val = node.get("exchange_passive", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid exchange_passive value")
            
        setattr(obj, "EXCHANGE_PASSIVE", val)
        
        val = node.get("exchange_durable", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid exchange_durable value")
            
        setattr(obj, "EXCHANGE_DURABLE", val)

        val = node.get("exchange_auto_delete", True)
        if not isinstance(val, bool):
            raise ValueError("Invalid exchange_auto_delete value")
            
        setattr(obj, "EXCHANGE_AUTO_DELETE", val)
        
        val = node.get("qos_prefetch_count", -1) #-1 - unlimmited
        if not isinstance(val, int) or val < -1 or val == 0:
            raise ValueError("Invalid qos_prefetch_count value")
            
        setattr(obj, "QOS_PREFETCH_COUNT", val)
        
        val = node.get("queue_off", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_off value")
        
        setattr(obj, "QUEUE_OFF", val)
            
        val = node.get("queue_name", "") #"" - non specific queues are admissible
            
        setattr(obj, "QUEUE_NAME", str(val))

        val = node.get("queue_exclusive", True)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_exclusive value")
            
        setattr(obj, "QUEUE_EXCLUSIVE", val)

        val = node.get("queue_passive", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_passive value")
            
        setattr(obj, "QUEUE_PASSIVE", val)

        val = node.get("queue_durable", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_durable value")
            
        setattr(obj, "QUEUE_DURABLE", val)
        
        val = node.get("queue_auto_delete", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_auto_delete value")
            
        setattr(obj, "QUEUE_AUTO_DELETE", val) 
        
        val = node.get("queue_message_ttl_ms")
        if val:
            val = int(val)
            
        setattr(obj, "QUEUE_MESSAGE_TTL_MS", val)
        
        val = node.get("queue_bind", True)
        if not isinstance(val, bool):
            raise ValueError("Invalid queue_bind value")
            
        setattr(obj, "QUEUE_BIND", val) 

        val = node.get("routing_key", "#")

        setattr(obj, "ROUTING_KEY", str(val))

        val = node.get("message_ttl_ms", 60000)

        setattr(obj, "MESSAGE_TTL_MS", int(val))

        val = node.get("no_ack", True)
        if not isinstance(val, bool):
            raise ValueError("Invalid no_ack value")
            
        setattr(obj, "NO_ACK", val)
        
        val = node.get("content_type", "application/json")

        setattr(obj, "CONTENT_TYPE", str(val))
                
        val = node.get("schema_validation_on", False)
        if not isinstance(val, bool):
            raise ValueError("Invalid schema_validation_on value")
            
        setattr(obj, "SCHEMA_VALIDATION_ON", val)
        
        val = node.get("schema", "")

        setattr(obj, "SCHEMA", str(val))
        
        obj.__init__(*args, **kwargs)
        return obj
    
'''
    //
    //------- CONFIG SECTION EXAMPLE
    //
    
    rabbitmq:
    { 
        connections:
        {
            api_bus:
            {
                host = "localhost";
                port = 5672;
                vhost = "/";
                login = "guest";
                password = "guest";
                frame_max = 131072; 
                heartbeat_s = 8; 
                reconnect_timeout_ms = 5000; 
            };
        };
    
        nodes:
        {
            api_1:
            {
                connection = "api_bus";
                
                exchange_name = "ex_server_A";
                exchange_type = "topic";
                qos_prefetch_count = -1;
                queue_off = false;
                queue_name = "q_server_A";
                queue_exclusive = true;
                route_key = "r_server_A"; 
                no_ack = false;
                content_type = "application/json";
                schema_validation_on = false;
                schema = "~/api.json";
            };           
        };
    };  
    
'''
