# -*- coding: utf-8 -*-

from ..libconfig import ConfGroup, ConfList
import re
import logging
import os.path as p
import os, sys

from wext_common.liblog import WLogger, LDBG, LWRN, LERR, LMSG

class _ConfLogger(object):    
    def __init__(self, conf, default_destinations = []):
        self.handlers = []            
        
        if isinstance(conf, ConfGroup):
            name = conf.get("name")
            
            nextConf = conf.get("handlers")
            if not nextConf:
                nextConf = conf.get("destinations")
            if not nextConf:
                raise ValueError("Section not found")
            
            if not isinstance(nextConf, list):
                raise TypeError("Invalid section type")
            for hdName in nextConf:
                if isinstance(hdName, str):
                    self.handlers.append(hdName)
        elif isinstance(conf, str):
            name = conf
        else:
            raise TypeError("Invalid config object")            
        
        self._name = r"\S+"                
        if isinstance(name, str) and \
            len(name):
            name = re.sub(r"(?<!\\)\.", r"\.", name)
            name = re.sub(r"(?<!\\)\?", r"\S{1}", name)
            name = re.sub(r"(?<!\\)\*$", r"\S+$", name)
            self._name = name
        self._rname = re.compile(self._name)
        
        LDBG("_ConfLogger = '%s'", self._name)

        self.handlers.extend(default_destinations)
                
    def pattern(self):
        return self._rname

class _StreamRedirector(object):
    
    old_sys_stream = None
    
    @staticmethod
    def overwrite(dst_loggers = []):
        old_sys_stream = sys.stdout
        sys_stream_obj = _StreamRedirector(logging.INFO, dst_loggers)
        sys.stdout = sys_stream_obj
        _StreamRedirector.old_sys_stream = old_sys_stream

    @staticmethod
    def restore():
        if _StreamRedirector.old_sys_stream:
            sys.stdout = _StreamRedirector.old_sys_stream
            _StreamRedirector.old_sys_stream = None
        
    def __init__(self, level, dst_loggers = []):
        self.dst_loggers = []        
        for loggerName in dst_loggers:
            loggerObj = logging.getLogger(loggerName)
            shortName = ConfLog._extractLoggerName(loggerObj)
            ConfLog.loggers[shortName] = shortName       
            self.dst_loggers.append(loggerObj)
        self.level = level

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            for logger in self.dst_loggers:
                logger.log(self.level, line.rstrip())
                
class _ConfHandler(object):
    
    _ConfLevels = {
        'debug': logging.DEBUG,
        'message': logging.INFO,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.ERROR,
            }
    
    def __init__(self, name, conf, default_type, default_level):
        self.name = name        
        if isinstance(conf, ConfGroup):
            self.type = conf.get("type", default_type)
            self.level = self._ConfLevels.get(conf.get("level", default_level), logging.NOTSET)
        else:
            self.type = default_type
            self.level = self._ConfLevels.get(default_level, logging.NOTSET)
        
    def configure(self, logger):
        raise NotImplementedError

class _ConfStreamHandler(_ConfHandler):
    def __init__(self, name, conf, default_type, default_level):
        super(_ConfStreamHandler, self).__init__(name, conf, default_type, default_level)
    def configure(self, logger):
        if isinstance(logger, WLogger):
            if self.type == 'stdout':
                tp = sys.stdout
            else:
                tp = sys.stderr
            LDBG("addStreamHandler {'%s', '%s'}", logger.name, self.type)
            if not isinstance(tp, _StreamRedirector):
                logger.addStreamHandler(
                    stream=tp, 
                    level=self.level)
            else:
                LWRN("addStreamHandler {'%s', '%s'} - ignored via std redirection", 
                     logger.name, self.type)
                

class _ConfSyslogHandler(_ConfHandler):
    def __init__(self, name, conf, default_type, default_level):
        super(_ConfSyslogHandler, self).__init__(name, conf, default_type, default_level)
    def configure(self, logger):
        if isinstance(logger, WLogger):
            LDBG("addLocalSysLogHandler {'%s'}", logger.name)
            logger.addLocalSysLogHandler(
                level=self.level)
            
class _ConfFileHandler(_ConfHandler):
    def __init__(self, name, conf, default_type, default_level):
        super(_ConfFileHandler, self).__init__(name, conf, default_type, default_level)
        self.rotate_count = conf.get("rotate_count", 1)
        self.max_size_mb = conf.get("max_size_mb", 10)
        self.filename = conf.get("filename", "")
    def configure(self, logger):
        if isinstance(logger, WLogger):
            LDBG("addRotatingFileHandler {'%s', '%s'}", logger.name, self.filename)
            logger.addRotatingFileHandler(
                filename = self._insurePath(self.filename), 
                maxBytes = self.max_size_mb*1024*1024, 
                backupCount = self.rotate_count,
                level=self.level)
    @staticmethod
    def _insurePath(filepath):
        if not isinstance(filepath, str):
            return ""
        filepath = p.normpath(filepath)
        if filepath.find("~") >= 0:
            filepath = p.expanduser(filepath)
        dirpath = p.dirname(filepath)
        if not p.exists(dirpath):
            os.makedirs(dirpath)
        return filepath
    
class ConfLog(object):
        
    _ConfHandlerCls = {
        'stdout': _ConfStreamHandler,
        'stderr': _ConfStreamHandler,
        'syslog': _ConfSyslogHandler,
        'file': _ConfFileHandler }

    #logger names for logging.getLogger(...)
    loggers = {}
    
    #options
    restoreLoggers = True

    def __init__(self, conf):
        
        _StreamRedirector.restore()
        if not self.restoreLoggers:
            self.loggers = {}
        
        #internal objects
        self._loggers = {}
        self._handlers = {}
        self._default_handler = _ConfStreamHandler("default", None, "stdout", "info")
        
        self._initHandlers(conf)
        self._initPatternLoggers(conf)
        
    def _initPatternLoggers(self, conf):
        if not isinstance(conf, ConfGroup):
            raise TypeError("Config object must be group")
        nextConf = conf.get("loggers")
        if not nextConf:
            nextConf = conf.get("systems")
        if not nextConf:
            raise ValueError("Section not found")
        default_logger = nextConf.get("name")            
        default_destinations = nextConf.get("destinations")
        if not isinstance(default_destinations, list):
            default_destinations = []
        if default_logger and default_destinations:
            self._loggers[default_logger] = _ConfLogger(default_logger, default_destinations)
        
        nextConf = nextConf.get("custom")
        if nextConf:
            if not isinstance(nextConf, ConfList):
                raise TypeError("Config object must be list")
            for logger in nextConf:
                if isinstance(logger, ConfGroup):
                    self._loggers[logger.name] = _ConfLogger(logger, default_destinations)

    def _initHandlers(self, conf):
        if not isinstance(conf, ConfGroup):
            raise AttributeError
                
        nextConf = conf.get("handlers")
        if not nextConf:
            nextConf = conf.get("destinations")
        if not nextConf:
            raise ValueError("Section not found")
        
        if conf.get("std_redirect") and \
           not nextConf.get("std_redirect"):
            setattr(nextConf, "std_redirect", conf.get("std_redirect"))
            
        default_type = nextConf.__dict__.get("type")
        if not isinstance(default_type, str):
            default_type = None
        default_level = nextConf.__dict__.get("level")
        if not isinstance(default_level, str):
            default_level = None
            
        redirect = nextConf.__dict__.get("std_redirect")
        if isinstance(redirect, list):
            _StreamRedirector.overwrite(redirect)
            
        for k, obj in nextConf.__dict__.items():
            if isinstance(obj, ConfGroup):
                hdType = obj.get("type", default_type)
                hdLevel = obj.get("level", default_level)
                hdObj = self._ConfHandlerCls.get(hdType)
                if hdObj:
                    self._handlers[k] = hdObj(k, obj, hdType, hdLevel)
    
    @staticmethod
    def _extractLoggerName(loggerObj):
        from .bind import WLoggerConf
        if isinstance(loggerObj, WLoggerConf):
            return loggerObj.shortName
        return loggerObj.name
        
    def configureLogger(self, loggerObj):
        shortName = self._extractLoggerName(loggerObj)
        self.loggers[shortName] = shortName
        bConfigured = False
        for logger in self._loggers.values():
            objMatch = logger.pattern().match(loggerObj.name)
            if objMatch:
                hds = [self._handlers.get(dst) for dst in logger.handlers if self._handlers.get(dst)]
                for hd in hds:
                    hd.configure(loggerObj)
                    bConfigured |= True
        if not bConfigured:
            self._default_handler.configure(loggerObj)
    
    def configure(self):
        '''
            Called by logging.config.dictConfig(cnf), 
            where cnf = ConfGroup for loggers
        '''
        logging.shutdown()
        
        from .bind import WLoggerConf
        WLoggerConf.configurator = self
        
        for name in self.loggers.values():
            logger = logging.getLogger(name)
            self.configureLogger(logger)
            
'''
    //
    //------- CONFIG SECTION EXAMPLE
    //
    logger:
    {
        std_redirect = ["STD"];

        handlers: //- new name, 'destinations' - obsolete name
        {
            level = "debug"; //debug, message, [info], warning, error, [critical]
            type="file"; //stdout, stderr, syslog, file
            std_redirect = ["STD"];
            
            std2:
            {
                level = "info";
                type = "stderr";
            };
            all:
            {
                level = "debug";
                type = "file";
                rotate_count = 5;
                max_size_mb = 200;
                filename="/tmp/app.logs/app.all";
            };
            brief:
            {
                rotate_count = 3;
                max_size_mb = 60;
                filename="/tmp/app.logs/app.brief";
            }
            short:
            {
                level = "message";
                type = "file";
                rotate_count = 3;
                max_size_mb = 100;
                filename="/tmp/app.logs/app";
            };
            debug:
            {
                rotate_count = 1;
                max_size_mb = 10;
                filename="/tmp/app.logs/app.debug";
            }
        };
        
        //loggers for test: 
        // "app",
        // "app.server", 
        // "app.server.consumer", 
        // "app.server.produser", 
        // "app.db"
        // "STD" (created automatically by 'std_redirect' instraction
        loggers: //- new name, 'systems' - obsolete name
        {
            handlers = ["all", "short"];
            custom = 
                (
                    //handlers for special loggers mask (overwrites 'handlers' in parent)
                    { 
                        name = "STD"; 
                        handlers = ["std2"];
                    },
                    { 
                        name = "app.server.*"; 
                        handlers = ["all", "short"]; 
                    },
                    { 
                        name = "app.server.produser"; 
                        handlers = ["all", "short", "brief"];
                    },
                    { 
                        name = "app.db"; 
                        handlers = ["debug"];
                    }
                );
        };
    };
'''
