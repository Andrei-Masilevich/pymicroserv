# -*- coding: utf-8 -*-

import wext_common.liblog #initializing wext logger for logging.getLoggerClass())
import logging
import logging.config as logging_config

class WLoggerConf(logging.getLoggerClass()):

    configurator = None
    
    def __init__(self, name = "", levelOpt = None):
        super(WLoggerConf, self).__init__( name, levelOpt)
        
        self.shortName = name
        from .config import ConfLog
        if self.configurator and isinstance(self.configurator, ConfLog):
            self.configurator.configureLogger(self)

def init(restoreLoggers = True):
    from .config import ConfLog

    #initialize binding
    ConfLog.restoreLoggers = restoreLoggers
    logging_config.dictConfigClass = ConfLog
    logging.setLoggerClass(WLoggerConf)
                
