#Lib: not executable
# -*- coding: UTF-8 -*-

from .helpers import _setPatternName

from wext_common.liblog import LDBG, LMSG, LWRN, LERR
import inspect

class Observable(object):
    
    def __init__(self, *args, **kwargs):
        LDBG("Observable - %r", hex(id(self)))

        _setPatternName(self.__dict__, Observable.__name__)
        self._observers = {}
        
    def connect(self, signalFunc, obj, slotFunc = None):

        LDBG("Observable::%s - %r", inspect.stack()[0][3], hex(id(self)))

        if not signalFunc or not obj:
            ValueError("Invalid arguments received")
        
        if slotFunc:
            _slotFunc = getattr(obj, slotFunc)
        else:
            _slotFunc = getattr(obj, signalFunc)
        
        if not _slotFunc:
            ValueError("Invalid function slot name received")

        objId = id(obj)
        observer = self._observers.get(objId)
        if not observer:
            self._observers[objId] = {}
            observer = self._observers.get(objId)
            
        observerSlot = observer.get(signalFunc)
        if not observerSlot:
            observer[signalFunc] = _slotFunc
        else:
            ValueError("Already connected")
        
        
    def disconnect(self, obj, signalFunc = None):
        LDBG("Observable::%s - %r", inspect.stack()[0][3], hex(id(self)))

        if not obj:
            ValueError("Invalid argument received")
        
        objId = id(obj)
        observer = self._observers.get(objId)
        if not observer:
            return True
        
        if signalFunc:
            if signalFunc in observer:
                observer.pop(signalFunc)
            else:
                return False
            
        if not signalFunc or not any(observer):
            self._observers.pop(objId)
        
        return True
    
    def fire(self, signalFunc, *args, **kwargs):
        LDBG("Observable::%s - %r", inspect.stack()[0][3], hex(id(self)))

        if not any(self._observers.keys()):
            LWRN("Observable::%s - %r - No any observers", inspect.stack()[0][3], hex(id(self)))

        for observerk in self._observers.keys():
            observer = self._observers.get(observerk)
            assert observer, "Object must be exist"
            slotFunc = observer.get(signalFunc)
            if slotFunc:
                slotFunc(*args, **kwargs)

