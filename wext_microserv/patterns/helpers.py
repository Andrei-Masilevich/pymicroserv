#Lib: not executable
# -*- coding: UTF-8 -*-

from tornado import gen

def reenterlock(func):
    def _reenterlock(*args,**kwargs):
        self = args[0]
        nameLock = "__lock_%s" % (func.__name__)
        lock = getattr(self, nameLock, False)
        if not lock:
            setattr(self, nameLock, True)
            r = func(*args,**kwargs)
            setattr(self, nameLock, False)
            return r
        else:
            return None
    return _reenterlock

def reenterlockEx(externalLockName = None):
    '''decodator with parameters'''
    def _reenterlockW(func):
        def _reenterlock(*args,**kwargs):
            self = args[0]
            if externalLockName:
                nameExternalLock = "__%s" % (str(externalLockName))
                lockExternal = getattr(self, nameExternalLock, False)
                if not lockExternal:
                    setattr(self, nameExternalLock, True)
                else:
                    return None
            nameLock = "__lock_%s" % (func.__name__)
            lock = getattr(self, nameLock, False)
            if not lock:
                setattr(self, nameLock, True)
                r = func(*args,**kwargs)
                setattr(self, nameLock, False)
                return r
            else:
                return None
        return _reenterlock
    return _reenterlockW

def reenterlockCoroutine(func):
    @gen.coroutine
    def _reenterlock(*args,**kwargs):
        self = args[0]
        nameLock = "__lock_%s" % (func.__name__)
        lock = getattr(self, nameLock, False)
        if not lock:
            setattr(self, nameLock, True)
            r = yield func(*args,**kwargs)
            setattr(self, nameLock, False)
            raise gen.Return(r)
    return _reenterlock


def _setPatternName(dct, name, attr_name = "_PATTERN", obj = None):
    lst = dct.get(attr_name)
    if not lst:
        if obj:
            setattr(obj, attr_name, [name])
        else:
            dct[attr_name] = [name]
    else:
        if not isinstance(lst, list):
            lst = [lst]
        if not name in lst:
            lst.append(name)
        if obj:
            setattr(obj, attr_name, lst)
        else:
            dct[attr_name] = lst