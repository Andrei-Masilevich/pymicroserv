#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_common.liblog import LDBG, LMSG, LWRN, LERR
from .helpers import _setPatternName

class Singleton(object):
    
    __pobj = {}

    def __new__(cls, *args, **kwargs):
        '''
        First case of usage. 
        
        If A inherits Singleton
        a = A(...)
        b = A(...)
        
        b same to a
        
        To prevent call __init__ repeatedly in b use _singleton_init
        '''

        return Singleton.instance(cls, *args, **kwargs)

    def _singleton_init(self, *args, **kwargs):
        if not type(self).__name__ in Singleton.__pobj:
            return True
        (pobj, obj_exit) = Singleton.__pobj.get(type(self).__name__)
        if not obj_exit:
            Singleton.__pobj[type(self).__name__] = (pobj, True)
            return True #constructor call allowed 
        return False
        
    @staticmethod
    def instance(cls, *args, **kwargs):
        '''
        Second case of usage.
    
        If A not inherits Singleton
        a = Singleton.instance(A, ...)
        b = Singleton.instance(A, ...)
    
        b same to a
        '''
        
        if cls.__name__ == 'type' or \
           cls.__name__ == 'object':
            return None
        if not cls.__name__ in Singleton.__pobj:
            Singleton.__pobj[cls.__name__] = (None, False)
            pobj = cls.__call__(*args, **kwargs)
            _setPatternName(cls.__dict__, Singleton.__name__, obj = pobj)
            Singleton.__pobj[cls.__name__] = (pobj, False)
            LDBG("~ Call %r", pobj)
        else:
            (pobj, nl) = Singleton.__pobj.get(cls.__name__)
            if not pobj:
                pobj = object.__new__(cls)
        return pobj