#Lib: not executable
# -*- coding: UTF-8 -*-

from wext_common.liblog import LDBG, LMSG, LWRN, LERR
from ..config import Config, ConfGroup
from .helpers import _setPatternName, reenterlock, reenterlockCoroutine

from tornado import gen
import tornado.ioloop

class _Tagging(object):
    def __init__(self, name, synonym = None):
        if not any(name):
            raise ValueError("Invalid name")
        self._name = name
        if synonym and any(synonym):
            self.synonym = synonym
        
    def __str__(self): return self._name
    def __cmp__(self, other):
        if not other:
            return 1
        return cmp(self._name, other._name)
    
    # Necessary when __cmp__ or __eq__ is defined
    # in order to make this class usable as a
    # dictionary key:    
    def __hash__(self):
        return hash(self._name)
            
class _State(_Tagging):
    def __init__(self, state, synonym):
        super(_State, self).__init__(state, synonym)
        self._transitions = {}
        self._allowedEvents = {}
        self._conditions = {}
        self._onEnterSlot = None
        self._onExitSlot = None
        self._onInvalidState = None
    
    def setup(self, transitions, allowedEvents, conditions = {}, onInvalidState = None):
        if transitions and not isinstance(transitions, dict):
            raise ValueError("Transitions objects are invalid")
        
        if allowedEvents and not isinstance(allowedEvents, dict):
            raise ValueError("Allowed events objects are invalid")
        
        if conditions and not isinstance(conditions, dict):
            raise ValueError("Conditions events objects are invalid")

        if not any(transitions) and not any(allowedEvents):
            raise ValueError("Transitions or allowed events not found")
        
        self._allowedEvents = allowedEvents
        
        for evt in transitions.keys():
            if not str(evt) in self._allowedEvents:
                self._allowedEvents[str(evt)] = evt
    
        if any(conditions):
            for evt in conditions.keys():
                if not str(evt) in self._allowedEvents:
                    raise ValueError("Invalid condition")
            self._conditions = conditions
            
        if any(transitions):
            self._transitions = transitions

        self._onInvalidState = onInvalidState
        
    def enter(self, *params):
        if self._onEnterSlot:
            self._onEnterSlot(*params)
        df = self._allowedEvents.get("default")
        if df and df in self._transitions:
            return self.next(df)
        return self
            
    def exit(self):
        if self._onExitSlot:
            self._onExitSlot()
    
    def registerCondition(self, event, condition):
        if condition and event and event in self._allowedEvents:
            self._conditions[event] = condition
        else:
            raise ValueError("Invalid condition or event")

    def unregisterCondition(self, event):
        if event and event in self._conditions:
            self._conditions.pop[event]
            
    def registerEnterSlot(self, onEnterSlot):
        if onEnterSlot:
            self._onEnterSlot = onEnterSlot
            
    def unregisterEnterSlot(self):
        self._onEnterSlot = None
        
    def registerExitSlot(self, onExitSlot):
        if onExitSlot:
            self._onExitSlot = onExitSlot
            
    def unregisterExitSlot(self):
        self._onExitSlot = None
        
    def next(self, event):
        if event in self._transitions:
            if event in self._conditions:
                cond = self._conditions[event]
                if not cond():
                    return self
            return self._transitions[event]
        elif str(event) in self._allowedEvents:
            return self
        else:
            if self._onInvalidState:
                self._onInvalidState(str(self), str(event))
            else:
                raise ValueError("Input not supported for current state")

class _Event(_Tagging):
    def __init__(self, event):
        super(_Event, self).__init__(event)

class SynchStateMachine(type):
    
    class _SynchStateMachineImpl(object):

        def __init__(self, *args, **kwargs):
            super(SynchStateMachine._SynchStateMachineImpl, self).__init__(*args, **kwargs)
        
        def sm_run(self, input_event, *params):
            self.sm_next(input_event, *params)
            
        def sm_isRunning(self):
            return True
        
        def sm_currentState(self):
            ret = str(self._currentState)
            if hasattr(self._currentState, "synonym"):
                ret += ".%s" % self._currentState.synonym
            return ret

        @reenterlock
        def sm_next(self, input_event, *params):
            event = self._events[input_event]
            nextState = self._currentState.next(event)
            while nextState != self._currentState:
                LDBG("~ Old state '%s'", self.sm_currentState())
                self._currentState.exit()
                self._currentState = nextState
                LDBG("~ New state '%s'", self.sm_currentState())
                nextState = self._currentState.enter(*params)
                    
        def sm_registerCondition(self, state, input_event, condition):
            event = self._events[input_event]
            self._states[state].registerCondition(event, condition)
    
        def sm_unregisterCondition(self, state, input_event):
            event = self._events[input_event]
            self._states[state].unregisterCondition(event)
            
        def sm_registerEnterSlot(self, state, onEnterSlot):
            self._states[state].registerEnterSlot(onEnterSlot)
    
        def sm_unregisterEnterSlot(self, state):
            self._states[state].unregisterEnterSlot()
            
        def sm_registerExitSlot(self, state, onExitSlot):
            self._states[state].registerExitSlot(onExitSlot)
    
        def sm_unregisterExitSlot(self, state):
            self._states[state].unregisterExitSlot()
    
    @staticmethod
    def _obtainState(states, name, synonym = None):
        state = None
        if not name in states:
            state = _State(name, synonym)
            states[name] = state
            if synonym and any(synonym):
                states[synonym] = state
        else:
            state = states.get(name)
            if synonym and any(synonym):
                state.synonym = synonym
        return state
       
    @staticmethod
    def _obtainEvent(events, name):
        event = None
        if not name in events:
            event = _Event(name)
            events[name] = event
        else:
            event = events.get(name)
        return event    
    
    def __call__(self, *args, **kwargs):
        obj = self.__new__(self, *args, **kwargs)
        
        obj._currentState = None
        obj._states = {}
        obj._events = {}

        sm_schema = kwargs.get("sm_schema")
        if not sm_schema:
            sm_schema = obj.sm_schema
            
        schemaInConfig = False
        sts = None
        
        if isinstance(sm_schema, list):
            sts = sm_schema
        else:
            name = kwargs.get("name")
            if not name:
                name = obj.name

            if isinstance(sm_schema, str):
                cf = Config(sm_schema)
                confObj = cf.lookup(name)
                if not confObj:
                    raise ValueError("Section not found")
                schemaInConfig = True
                sts = confObj.get("states")
            elif isinstance(sm_schema, dict):
                sts = sm_schema.get(name)
            else:
                raise TypeError("SM schema has invalid type")
            
        if not sts:
            raise ValueError("Invalid SM schema")

        for st in sts:

            stName = st.get("id")
            if not stName or not any(stName):
                raise ValueError("Invalid state name")
            
            state = self._obtainState(obj._states, stName, st.get("synonym"))
            
            evts = st.get("events")
            if evts and not isinstance(evts, list):
                raise ValueError("Events not found (or invalid)")
            
            allowedEvents = {}
            if evts:
                for evtName in evts:
                    event = self._obtainEvent(obj._events, evtName)
                    allowedEvents[evtName] = event
            
            trss = []
            trss.append(st.get("transitions"))
            if stName != "error":
                if schemaInConfig:
                    trss.append([ConfGroup({'event': "error", "target": "error"})])
                else:
                    trss.append([{'event': "error", "target": "error"}])
            
            transitions = {}
            
            for trs in trss:
                for tr in trs:
                    
                    targetName = tr.get("target")
                    if not targetName or not any(targetName):
                        raise ValueError("Invalid target name")
                    
                    targetState = self._obtainState(obj._states, targetName)
                    if not targetState:
                        raise ValueError("Invalid target")
                    
                    evtName = tr.get("event")
                    if not evtName or not any(evtName):
                        evtName = "default"
                    
                    event = self._obtainEvent(obj._events, evtName)
                    transitions[event] = targetState
            
            _onInvalidState = kwargs.get("onInvalidState")
            state.setup(transitions, allowedEvents, onInvalidState = _onInvalidState)
        
        initialState = obj._states.get("initial")
        if not initialState:
            raise ValueError("Initial state not found")
            
        obj._currentState = initialState
        obj.__init__(*args, **kwargs)
        return obj
            
    def __new__(self, name, bases, dct):
        newBase = [self._SynchStateMachineImpl]
        newBase.extend(list(bases))
        _setPatternName(dct, self.__name__)
        return type.__new__(self, name, tuple(newBase), dct)

class AsynchStateMachine(SynchStateMachine):
    
    class _AsynchStateMachineImpl(SynchStateMachine._SynchStateMachineImpl):
        def __init__(self, *args, **kwargs):
            super(AsynchStateMachine._AsynchStateMachineImpl, self).__init__(*args, **kwargs)

        #@reenterlockCoroutine
        @gen.coroutine
        def sm_next(self, input_event, *params):
            event = self._events[input_event]
            nextState = self._currentState.next(event)
            loopState = self._currentState
            ci = 0
            while nextState != loopState:
                LMSG("~ Old state '%s' [%d]", self.sm_currentState(), ci)
                self._currentState.exit()
                self._currentState = nextState
                loopState = self._currentState 
                LMSG("~ New state '%s' [%d]", self.sm_currentState(), ci)
                nextState = self._currentState.enter(*params)
                ci = ci + 1
                yield None
      
        def sm_run(self, input_event, *params):
            
            io_loop = tornado.ioloop.IOLoop.current()
            
            assert io_loop._running, "IOLoop must be runned"
            
            super(AsynchStateMachine._AsynchStateMachineImpl, self).sm_run(input_event, *params)
        
        def sm_isRunning(self):
            if not super(AsynchStateMachine._AsynchStateMachineImpl, self).sm_isRunning():
                return False

            io_loop = tornado.ioloop.IOLoop.current()
            
            return io_loop._running
        
    def __new__(self, name, bases, dct):
        newBase = [self._AsynchStateMachineImpl]
        newBase.extend(list(bases))
        _setPatternName(dct, self.__name__)
        return type.__new__(self, name, tuple(newBase), dct)

        
'''
    //
    //------- CONFIG STATE MACHINE EXAMPLE
    //
    
    myStateMachineApp:
    { 
        states = 
        (
            {
                id = "initial"; //reserved word for intial state
                
                synonym = "waiting";
                
                /*
                  Allowed events for this state. 
                  Not all make transitions.
                  If smth. calls state event that is not mentioned in lists
                  or in transitions, that event is not allowed
                */
                events = ("go", "idle", "stop");
                
                transitions = 
                (
                    {
                        event = "go";
                        
                        target = "s2";
                    },
                    {
                        event = "about"; 
                        /* not all event may be mentioned in events property
                           (it is allowed obviously)
                        */
                        target = "s3";
                    },
                    //"idle" and "stop" events just left current state
                );
            },
            {
                id = "s2";
                
                synonym = "idle";
                
                events = ("idle", "stop");
                
                transitions = 
                (
                    {
                        event = "stop";
                        target = "final";
                    },
                );
            },
            {
                id = "s3";
                
                events = ("stop");
                
                transitions = 
                (
                    {
                        /* 
                          DEFAULT event 
                          (event is not required to move to next state)
                        */
                        event = "default"; //reserved word
                        target = "final";
                    },
                    {
                        /* 
                          DEFAULT transition
                        */
                        event = "error";
                        target = "error";
                    },
                );
            },
            {
                id = "final"; //reserved word for final state
                
                synonym = "stopped";
                
                transitions = 
                (
                    {
                        //Artificially loop state machine
                        target = "initial";
                    },
                );
            },
            {
                /* 
                  DEFAULT state
                */
                id = "error"; //reserved word for error state
                
                transitions = 
                (
                    {
                        event = "reset";
                        target = "initial";
                    },
                );
            },
        );
    };  
    
'''
