# #!/usr/bin/python

import fcntl
import os
import pwd
import grp
import sys
import signal
import resource
import logging
import atexit
from logging import handlers
import traceback
import os.path as p
import inspect
import psutil

from wext_common.liblog import LDBG, LMSG, LWRN, LERR

class Daemonize(object):
    """
    Daemonize object.

    Object constructor expects three arguments.

    :param app: contains the application name which will be sent to syslog.
    :param pid: path to the pidfile.
    :param action_on_running: your custom function which will be executed after daemonization.
    :param action_on_stopping: your custom function which will be executed after signal interruption.
    :param keep_fds: optional list of fds which should not be closed.
    :param auto_close_fds: optional parameter to not close opened fds.
    :param privileged_action: action that will be executed before drop privileges if user or
                              group parameter is provided.
                              If you want to transfer anything from privileged_action to action, such as
                              opened privileged file descriptor, you should return it from
                              privileged_action function and catch it inside action function.
    :param user: drop privileges to this user if provided.
    :param group: drop privileges to this group if provided.
    :param logger: use this logger object instead of creating new one, if provided.
    :param foreground: stay in foreground; do not fork (for debugging)
    :param chdir: change working directory if provided or /
    """
    def __init__(self, 
                 app, 
                 action_on_running, 
                 action_on_stopping,
                 stop_signals = [signal.SIGINT, signal.SIGTERM],
                 pid = None, 
                 keep_fds = None, 
                 auto_close_fds = True, 
                 privileged_action = None,
                 user = None, 
                 group = None, 
                 foreground = False, 
                 chdir = None):
        
        if sys.platform.startswith("win"):
            raise NotImplemented("Not implemented on Windows platform")
            
        if pid and not p.isabs(pid):
            raise ValueError("Invalid PID file path")

        if not action_on_running:
            raise ValueError("No callback")
            
        self.app = app
        self.pid = pid
        self.action_on_running = action_on_running
        self.action_on_stopping = action_on_stopping or (lambda exit_code: sys.exit(exit_code))
        self.stop_signals = stop_signals
        self.keep_fds = keep_fds or []
        self.privileged_action = privileged_action or (lambda: ())
        self.user = user
        self.group = group
        self.auto_close_fds = auto_close_fds
        self.foreground = foreground
        self.chdir = chdir

    def sigterm(self, signum, frame):
        """
        These actions will be done after SIGTERM.
        """
        LMSG("Caught signal %s. Stopping daemon.", signum)
        self.action_on_stopping(0)

    def onCleanup(self, pid):
        """
        Cleanup pid file.
        """
        LMSG(inspect.stack()[0][3])
        
        if p.exists(pid):
            os.remove(pid)

    def start(self):
        """
        Start daemonization process.
        """
        ret = self._start()
        if ret and self.pid:
            self.onCleanup(self.pid)
        return ret

    def _start(self):
        if self.pid:
            old_pid = None
            # If pidfile already exists, we should read pid from there; to overwrite it, if locking
            # will fail, because locking attempt somehow purges the file contents.
            if os.path.isfile(self.pid):
                with open(self.pid, "r") as old_pidfile:
                    old_pid = old_pidfile.read()
                    
            if old_pid and old_pid not in psutil.pids():
                self.onCleanup(old_pid)
            
            # Create a lockfile so that only one instance of this daemon is running at any time.
            try:
                lockfile = open(self.pid, "w")
            except IOError:
                LERR("Unable to create the pidfile.")
                return False
            try:
                # Try to get an exclusive lock on the file. This will fail if another process has the file
                # locked.
                fcntl.flock(lockfile, fcntl.LOCK_EX | fcntl.LOCK_NB)
            except IOError:
                if old_pid:
                    LERR("Unable to lock on the pidfile. Check if previous instance %s in work", old_pid)
                    # We need to overwrite the pidfile if we got here.
                    with open(self.pid, "w") as pidfile:
                        pidfile.write(old_pid)
                else:
                    LERR("Unable to lock on the pidfile.")
                return False

        # skip fork if foreground is specified
        if not self.foreground:
            # Fork, creating a new process for the child.
            try:
                process_id = os.fork()
            except OSError as e:
                LERR("Unable to fork, errno: {0}".format(e.errno))
                return False
            if process_id != 0:
                # This is the parent process. Exit without cleanup,
                # see https://github.com/thesharp/daemonize/issues/46
                os._exit(0)
            # This is the child process. Continue.

            # Stop listening for signals that the parent process receives.
            # This is done by getting a new process id.
            # setpgrp() is an alternative to setsid().
            # setsid puts the process in a new parent group and detaches its controlling terminal.
            process_id = os.setsid()
            if process_id == -1:
                # Uh oh, there was a problem.
                return False

            if self.pid:
                # Add lockfile to self.keep_fds.
                self.keep_fds.append(lockfile.fileno())

            # Close all file descriptors, except the ones mentioned in self.keep_fds.
            devnull = "/dev/null"
            if hasattr(os, "devnull"):
                # Python has set os.devnull on this system, use it instead as it might be different
                # than /dev/null.
                devnull = os.devnull

            if self.auto_close_fds:
                for fd in range(3, resource.getrlimit(resource.RLIMIT_NOFILE)[0]):
                    if fd not in self.keep_fds:
                        try:
                            os.close(fd)
                        except OSError:
                            pass

            try:
                devnull_fd = os.open(devnull, os.O_RDWR)
                os.dup2(devnull_fd, 0)
                os.dup2(devnull_fd, 1)
                os.dup2(devnull_fd, 2)
                os.close(devnull_fd)
            except Exception as e:
                LERR("input/output redirection failed: %s", e)
                pass

        # Set umask to default to safe file permissions when running as a root daemon. 027 is an
        # octal number which we are typing as 0o27 for Python3 compatibility.
        os.umask(0o27)

        # Change to a known directory. If this isn't done, starting a daemon in a subdirectory that
        # needs to be deleted results in "directory busy" errors.
        if self.chdir:
            os.chdir(self.chdir)

        # Execute privileged action
        privileged_action_result = self.privileged_action()
        if not privileged_action_result:
            privileged_action_result = ()
        elif not isinstance(privileged_action_result, tuple):
            privileged_action_result = (privileged_action_result)

        # Change owner of pid file, it's required because pid file will be removed at exit.
        uid, gid = -1, -1

        if self.group:
            try:
                gid = grp.getgrnam(self.group).gr_gid
            except KeyError:
                LERR("Group '%s' not found", self.group)
                return False

        if self.user:
            try:
                uid = pwd.getpwnam(self.user).pw_uid
            except KeyError:
                LERR("User '%s' not found.", self.user)
                return False

        if self.pid:
            if uid != -1 or gid != -1:
                os.chown(self.pid, uid, gid)

        # Change gid
        if self.group:
            try:
                os.setgid(gid)
            except OSError:
                LERR("Unable to change gid.")
                return False

        # Change uid
        if self.user:
            try:
                uid = pwd.getpwnam(self.user).pw_uid
            except KeyError:
                LERR("User '%s' not found.", self.user)
                return False
            try:
                os.setuid(uid)
            except OSError:
                LERR("Unable to change uid.")
                return False

        if self.pid:
            try:
                lockfile.write("%s" % (os.getpid()))
                lockfile.flush()
            except IOError:
                LERR("Unable to write pid to the pidfile.")
                return False

        # Set custom action on SIGTERM.
        for sig_num in self.stop_signals:
            signal.signal(sig_num, self.sigterm)
        if self.pid:
            atexit.register(self.onCleanup, self.pid)

        LMSG("Starting daemon.")

        try:
            self.action_on_running(*privileged_action_result)
        except SystemExit:
            pass
        except Exception:
            raise
        
        return True
