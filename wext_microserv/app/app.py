#Lib: not executable
# -*- coding: UTF-8 -*-

import logging
import traceback
import sys
import signal
import os
import time
import os.path as p
import datetime
import tornado.ioloop
from tornado.ioloop import PeriodicCallback
from tornado import gen

from wext_common.liblog import LDBG, LMSG, LWRN, LERR
from wext_common.libsys import isWin
    
from .env import Env
from .serviceNtf import NoneNtf, SystemdNtf, ReviverNtf
from ..patterns import AsynchStateMachine, Singleton

import inspect

class AppImpl(object):
    '''
    Implementation for business logic. May have private state machine
    '''
    #program initialization steps for
    # -- additional command line arguments
    def onArgumentsInit(self, app, parser):
        pass #prepare args
    # -- additional configuration
    def onConfigInit(self, app, args, confObj):
        pass #create loggers and etc.
    # -- additional operations while daemon starting
    def onDaemonInit(self, app, args):
        return () #*daemon_params
    
    #occur single time while program starting
    @gen.coroutine
    def onStart(self, app, args, *daemon_params):
        yield None
        raise gen.Return(True)
    
    #occur while program started or after configuration completed
    @gen.coroutine
    def onRun(self, app, args):
        yield None
        raise gen.Return(True)
    
    #occur while program configuting
    @gen.coroutine
    def onConfigReload(self, app, args, confObj):
        yield None #pass on loop only
        raise gen.Return(True)
    
    #occur while program stopping
    @gen.coroutine
    def onStop(self, app, args):
        yield None
        raise gen.Return(True)
        
class App(Singleton, metaclass = AsynchStateMachine):
    
    app = None
    
    sm_schema = [
        {
            'id': 'initial',
            'synonym': 'stopped',
            'events': ['stop'], #ignored
            'transitions': 
            [
                {
                    'event': 'run',
                    'target': 'starting'
                }
            ]
        },
        {
            'id': 'starting',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'run',
                    'target': 'running'
                },
                {
                    'event': 'stop',
                    'target': 'stopping'
                }
            ]
        },
        {
            'id': 'running',
            'events': ['run'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'stop',
                    'target': 'stopping'
                },
                {
                    'event': 'config',
                    'target': 'configuring'
                }
            ]
        },
        {
            'id': 'configuring',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'run',
                    'target': 'running'
                },
                {
                    'event': 'stop',
                    'target': 'stopping'
                }
            ]
        },
        {
            'id': 'stopping',
            'events': ['config'], #ignored
            'transitions': 
            [ 
                {
                    'event': 'stop',
                    'target': 'stopped'
                }
            ]
        },
        {
            'id': 'error',
            'transitions': 
            [ 
                {
                    'target': 'stopped'
                }
            ]
        }
    ]
    
    enResultOk = 0
    enResultError = 1
    enResultErrorInvalidArgs = 2
    enResultErrorInvalidConfig = 102
    enResultErrorCantDaemonize = 103
    enResultInterrupt = 111
            
    def __init__(self, 
                 impl_cls,
                 name, 
                 description, 
                 rootPath, 
                 defaultConfigPath):
        
        if self._singleton_init():
        
            if not impl_cls or not AppImpl in impl_cls.__bases__:
                raise ValueError("Invalid constructor args")
            
            App.app = self
            
            Env.init(name, rootPath)
            
            self._impl_cls = impl_cls
            self._app_description = description
            self._defaultConfigPath = defaultConfigPath
            self._daemon_params = ()
            
            self._serviceNotifier = NoneNtf()
    
            self.log = None
            
            self._args = []
            self._timers = {}
            self._ctimers = 0
            self._err_code = self.enResultOk

            self.sm_registerEnterSlot('starting', self.onStarting)
            self.sm_registerEnterSlot('running', self.onRunning)
            self.sm_registerEnterSlot('configuring', self.onConfiguring)
            self.sm_registerEnterSlot('stopping', self.onStopping)
            self.sm_registerEnterSlot('stopped', self.onStopped)
    
    def _next(self, event:str, *params):
        LMSG("---state = {}, _next({},{})".format(self.sm_currentState().split('.')[0], event, params))
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.add_callback(self.sm_next, event, *params)
        
    def name(self):
        return Env.instance.appName
    
    def inBundle(self):
        return Env.instance.inBundle
    
    def _clear(self, err_code = None):
        keys = self._timers.keys()
        keys_ = list(keys)
        for tmId in keys_:
            self.removeTimer(tmId)
        self._args = []
        self._timers = {}
        self._ctimers = 0
        if err_code:
            self._err_code = err_code
        else:
            self._err_code = self.enResultOk
        
    def _printVersion(self):
            
        ver = Env.instance.getVersion()

        LMSG("%s %s", self.name(), ver)

    def _argumentsInit(self, args):
        import argparse #are not included in Python 2.6.6
        import sys
        
        try:            
            parser = argparse.ArgumentParser( prog = self.name(),
                                              description = self._app_description )
                
            parser.add_argument( "-v", "--version", 
                                 action = "store_true" )
        
            parser.add_argument( "-c", "--config", 
                                 type = str, 
                                 help="Path to configuration file" )
            
            b_default=None
            
            parser.add_argument("-b", "--daemon", 
                                action = "store_true",
                                default = b_default,
                                help="Launch as a daemon")
                
            class BAction(argparse.Action):
                def __call__(self,parser,namespace,values,option_string=None):
                    didb = getattr(namespace,"daemon",b_default)
                    if(didb == b_default):
                        parser.error( "'daemon' option required!")
                    else:
                        setattr(namespace,self.dest,values)

            parser.add_argument("-p", "--pid", 
                                type = str,
                                action=BAction,
                                help="PID file path or folder (only use this if 'daemon' is set)")
            parser.add_argument("-d", "--chdir", 
                                type = str,
                                action=BAction,
                                help="Change current directory (only use this if 'daemon' is set)")
            parser.add_argument("-u", "--user", 
                                type = str,
                                action=BAction,
                                help="To give process to user (only use this if 'daemon' is set)")
            parser.add_argument("-g", "--group", 
                                type = str,
                                action=BAction,
                                help="To give process to group (only use this if 'daemon' is set)")
            
            Singleton.instance(self._impl_cls).onArgumentsInit(self, parser)
            
            _args = args
            if not _args or not len(_args):
                _args = sys.argv[1:]
            self._args = parser.parse_args( _args )
            
            if not self._args.version and \
               not self._args.config:
                raise ValueError("Invalid command line args")
            
        except SystemExit:
            return False
        except Exception:
            parser.print_help(sys.stderr)
            LDBG("Got '%s'", self._args)
            for line in traceback.format_exc().split("\n"):
                LERR(line)
            return False
        
        return True
    
    def _configure(self, confDataStr = None, init = True):
        
        from ..config import Config
        
        confObj = None
        
        if confDataStr and any(confDataStr): 
            try:
                cf = Config(confDataStr)
                confObj = cf.lookup(self.name())
            except Exception:
                LWRN("Invalid configuration string")
                for line in traceback.format_exc().split("\n"):
                    LDBG(line)
        else:
            cf = Config()
            try:
                conf_path = p.normpath(self._args.config)
                if conf_path.lower() != "default":
                    if conf_path.find("~") >= 0:
                        conf_path = p.expanduser(conf_path)
                    if not p.exists(conf_path):
                        raise IOError("Configuration file is not exist")
                    cf.read_file(conf_path)
                    confObj = cf.lookup(self.name())
            except Exception:
                LWRN("Invalid configuration file '%s'", self._args.config)
                for line in traceback.format_exc().split("\n"):
                    LDBG(line)
        
        if not confObj and self._defaultConfigPath:
            cf = Config()
            try:
                conf_path = p.normpath(self._defaultConfigPath)
                if not p.exists(conf_path):
                    raise IOError("Default configuration file is not exist")
                cf.read_file(conf_path)
                confObj = cf.lookup(self.name())
            except Exception:
                LERR("Invalid default configuration '%s'", self._defaultConfigPath)
                return None
                    
        try:
            if not confObj:
                raise ValueError("There is no any configuration")
            
            conf_log_block = confObj.get("logger")
            if conf_log_block:
                import logging.config
                logging.config.dictConfig(conf_log_block)
            
            self.log = logging.getLogger(self.name())
            
            if init:
                self.log.debug(os.environ)
            
            self.log.info("Configuration:\n %s", str(confObj))
            
            if not isWin():
                watchdog = confObj.get("watchdog")
                if watchdog == "systemd":
                    self._serviceNotifier = SystemdNtf(self.name())
            
            if init:
                Singleton.instance(self._impl_cls).onConfigInit(self, self._args, confObj)
            
        except Exception:
            LWRN("Invalid configuration")
            for line in traceback.format_exc().split("\n"):
                LDBG(line)
            return None
        
        return confObj
    
    def _getSecurity(self):
        user = "unknown"
        group = "unknown"
        
        if not isWin():
            import pwd
            import grp        
            try:
                user = pwd.getpwuid(os.getuid()).pw_name
            except Exception:
                pass
            try:
                group = grp.getgrgid(os.getgid()).gr_name
            except Exception:
                pass
        else:
            import getpass
            try:
                user = getpass.getuser()
            except Exception:
                pass
            group = user
            
        return (user, group)
        
    def _daemonize(self, pid, user, group, chdir):

        if isWin():
            #Not emplemented
            LWRN("Daemonize is not supported on platform '%s'", sys.platform)
            return False

        (current_user, current_group) = self._getSecurity()
        
        if current_user != current_group:
            LMSG("Daemonizing is starting under '%s:%s'", current_user, current_group)
        else:
            LMSG("Daemonizing is starting under '%s'", current_user)

        from .daemonize import Daemonize as D

        if not chdir:
            
            if current_user == "root":
                chdir = "/"
            else:
                chdir = p.expanduser("~")
        elif chdir.startwith("~"):
            chdir = p.expanduser(chdir)
        else:
            chdir = p.abspath(chdir)
        
        foreground = (True if os.environ.get("WEXT_COMMON_DEBUG") else False)
        daemon = D(self.name(), 
                   pid = pid, 
                   user = user,
                   group = group,
                   chdir = chdir,
                   auto_close_fds = False, #keep config and log files fds
                   privileged_action = self._privileged_action,
                   action_on_running = self._run,
                   action_on_stopping = self._sig_interruption_impl,
                   foreground = foreground,
                   stop_signals = [signal.SIGINT, signal.SIGTERM, signal.SIGUSR1])
        
        return daemon.start()

    def _privileged_action(self):
        ret = [True] #daemon mark
        self._daemon_params = Singleton.instance(self._impl_cls).onDaemonInit(self, self._args)
        if any(self._daemon_params):
            ret.extend(list(self._daemon_params))
        return tuple(ret)
    
    def isDaemonized(self):
        return any(self._daemon_params)
    
    def _run(self, *ignore):
        if not self._configure():
            raise ValueError("Configuration error")
        self._runLoop()
        
    def _runLoop(self):
        if not isWin():
            LMSG("Registration for SIGUSR2")
            signal.signal(signal.SIGUSR2, self._sig_config)
            if not self.isDaemonized():
                LMSG("Registration for SIGINT")
                signal.signal(signal.SIGINT, self._sig_interruption)
                LMSG("Registration for SIGTERM")
                signal.signal(signal.SIGTERM, self._sig_interruption)
                LMSG("Registration for SIGUSR1")
                signal.signal(signal.SIGUSR1, self._sig_interruption)
        
        (current_user, current_group) = self._getSecurity()
        
        assert self.log, "Log must be initiated"
        
        if current_user != current_group:
            self.log.info("Service is starting under '%s:%s'", current_user, current_group)
        else:
            self.log.info("Service is starting under '%s'", current_user)
            
        io_loop = tornado.ioloop.IOLoop.current()
        assert not io_loop._running, "Event loop already started"
        self._next("run")
        io_loop.start()
    
    def _sig_interruption_impl(self, exit_code):
        if self.isRunning():
            io_loop = tornado.ioloop.IOLoop.current()
            if not exit_code:
                io_loop.add_callback_from_signal(self.stop)
            else:
                io_loop.add_callback_from_signal(self.error)
        elif self.log:
            self.log.warning("Ignored in state '%s'", self.sm_currentState())
    
    def _sig_interruption(self, signum, frame):
        LMSG("Caught signal %s. Stopping.", signum)
        self._sig_interruption_impl(0)
        
    def _sig_config(self, signum, frame):
        
        LMSG("Caught signal %s. Configurate.", signum)
        
        if self.isRunning():
            io_loop = tornado.ioloop.IOLoop.current()
            io_loop.add_callback_from_signal(self.configure)
        elif self.log:
            self.log.warning("Ignored in state '%s'", self.sm_currentState())
    
    def isStarting(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'starting'
    
    def isRunning(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'running'
    
    def isConfiguring(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'configuring'
    
    def isStopping(self):
        if not self.sm_isRunning():
            return False
            
        return self.sm_currentState().split('.')[0] == 'stopping'
    
    def _getPID(self, input_pid):
        ret = None
        if input_pid and any(input_pid):
            _input_pid = input_pid
            if _input_pid.startswith("~"):
                _input_pid = p.expanduser(_input_pid)
            if _input_pid.endswith("/"):
                _input_pid = p.join(_input_pid, self.name())
                ret = p.abspath(_input_pid)
            elif p.dirname(_input_pid):
                ret = p.abspath(_input_pid)
            else:
                ret = p.join("/tmp", self.name())

        return ret
    
    def run(self, args = []):
        LMSG("%s()", inspect.stack()[0][3])
        if self.sm_isRunning():
            self._next('run')
            return
            
        try:
            self._clear()

            if not self._argumentsInit(args):
                LERR("Can't extract required arguments")
                return self.enResultErrorInvalidArgs
            
            if self._args.version:
                self._printVersion()
                return self.enResultOk
            
            from ..config import init as logger_init
            
            logger_init(restoreLoggers = False)

            if self._args.daemon and not isWin():
                if not self._daemonize(pid = self._getPID(self._args.pid),
                                       user = self._args.user,
                                       group = self._args.group,
                                       chdir = self._args.chdir):
                    LERR("Can't daemonize")
                    return self.enResultErrorCantDaemonize
            else:
                if self._args.daemon and isWin():
                    LWRN("Can't daemonize on Windows. Ignored")
                self._run()

        except SystemExit:
            pass
        except Exception:
            for line in traceback.format_exc().split("\n"):
                LERR(line)
            return self.enResultError
        except KeyboardInterrupt:
            app_ = Singleton.instance(self._impl_cls)
            del app_
            return self.enResultInterrupt
        
        return self._err_code
    
    def configure(self, confDataStr = ""):
        '''This method is called automatically.
        But could be called by external configuration mechanism 
        (For instance, by web manager)
        confDataStr - external configuration data string in libconfig format'''
        
        self._next('config', confDataStr)
    
    def stop(self):
        '''This method is called automatically (by signals).
        But could be called in onXXX handles'''
        
        self._next('stop')
        
    def error(self, err_code = None):
    
        if err_code:
            self._err_code = err_code
        else:
            self._err_code = self.enResultError
        
        if self.sm_isRunning():
            self._next('error') #to halt from current normal state
        else:
            raise ValueError("Application Error")
    
    @gen.coroutine
    def onStarting(self):
        LMSG("%s()", inspect.stack()[0][3])
        self.log.debug("%s()", inspect.stack()[0][3])
        
        if self._serviceNotifier and self._serviceNotifier.kaPeriodMS():
            self.addTimer(self._serviceNotifier.kaPeriodMS(), self.notifyKeepAlive, repeat = True)
        
        result = False
        try:
            result = yield Singleton.instance(self._impl_cls).onStart(self, self._args, *self._daemon_params)
            if result:
                self.run()
            else:
                raise ValueError("Can't start")
        except ValueError as e:
            self.log.error(e)
        except Exception:
            for line in traceback.format_exc().split("\n"):
                self.log.error(line)
                
        if not result:
            self.error(self.enResultError)
    
    @gen.coroutine
    def onRunning(self):
        LMSG("%s()", inspect.stack()[0][3])
        self.log.debug("%s()", inspect.stack()[0][3])
        
        self._serviceNotifier.notifyStarted()
        
        result = False
        try:
            result = yield Singleton.instance(self._impl_cls).onRun(self, self._args)
        except ValueError as e:
            self.log.error(e)
        except Exception:
            for line in traceback.format_exc().split("\n"):
                self.log.error(line)
        
        if not result:
            self.error(self.enResultError)
    
    @gen.coroutine
    def onConfiguring(self, confDataStr):
        LMSG("%s()", inspect.stack()[0][3])
        self.log.debug("%s()", inspect.stack()[0][3])
        
        self._serviceNotifier.notifyReloadStart()
        
        result = False
        try:
            confObj = self._configure(confDataStr, False)
            result = yield Singleton.instance(self._impl_cls).onConfigReload(self, self._args, confObj)
            if result:
                self.run()
            else:
                raise ValueError("Can't configurate")
        except ValueError as e:
            self.log.error(e)
        except Exception as e:
            self.log.error('%r', e)
            
        if not result:
            self._serviceNotifier.notifyMessage("Invalid configuration")
            self.error(self.enResultErrorInvalidConfig)
        else:
            assert self.log, "Log must be initiated!"

    @gen.coroutine
    def onStopping(self):
        LMSG("%s()", inspect.stack()[0][3])
        self.log.debug("%s()", inspect.stack()[0][3])
        
        self._clear(self._err_code)
        
        result = False
        try:
            result = yield Singleton.instance(self._impl_cls).onStop(self, self._args)
            if result:
                self.stop()
            else:
                raise ValueError("Can't stop")
        except ValueError as e:
            self.log.warning(e)
        except Exception as e:
            self.log.warning('%r', e)
            
        if not result:
            self._serviceNotifier.notifyMessage("Can't stop correctly")
            self.error(self.enResultError)
    
    def onStopped(self):
        LMSG("%s()", inspect.stack()[0][3])
        self.log.debug("%s()", inspect.stack()[0][3])
        
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.stop()
        assert self.log, "Log must be initiated!"
        self.log.info("=")
        sys.exit(self._err_code)

    def _onTimerCallback(self, tmId, callback, *params):
        if tmId in self._timers:
            self._timers.pop(tmId)
        callback(*params)
        
    def addTimer(self, periodMS, onTimerCallback, repeat = False, *params):
        if periodMS < 0 or not onTimerCallback:
            return -1
        
        io_loop = tornado.ioloop.IOLoop.current()
        
        self._ctimers += 1
        
        if not repeat:
            _params = list(params)
            _params.insert(0, onTimerCallback)
            _params.insert(0, self._ctimers)
            tm = io_loop.add_timeout(datetime.timedelta(milliseconds=periodMS), self._onTimerCallback, *_params)
        else:
            _onTimerCallback = lambda params = params: onTimerCallback(*params)
            tm = PeriodicCallback(_onTimerCallback, periodMS)
            tm.start()
        
        self._timers[self._ctimers] = tm
        return self._ctimers
        
    def removeTimer(self, tmId):
        if tmId < 1:
            return
        
        tm = self._timers.get(tmId)
        if not tm:
            self.log.warn("Timer %d not found", tmId)
            return
        
        try:
            if isinstance(tm, PeriodicCallback):
                tm.stop()
                del tm
            else:
                io_loop = tornado.ioloop.IOLoop.current()
                io_loop.remove_timeout(tm)
            self._timers.pop(tmId)
        except Exception:
            for line in traceback.format_exc().split("\n"):
                LERR(line)

    def notifyKeepAlive(self):
        self._serviceNotifier.notifyKeepAlive(True)
        
 