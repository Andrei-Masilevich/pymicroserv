#Lib: not executable
# -*- coding: UTF-8 -*-

import os
import imp

class Env(object):

    instance = None

    def getDEF(self, name):

        deflists = [self.appName + "_deflist.py", "deflist.py"]
        
        py_mod = None
        for defFileName in deflists:
            
            mod_name, file_ext = os.path.splitext(os.path.split(defFileName)[-1])
    
            try:    
                if file_ext.lower() == '.py':
                    py_mod = imp.load_source(mod_name, defFileName)
                elif file_ext.lower() == '.pyc':
                    py_mod = imp.load_compiled(mod_name, defFileName)
                    
                if py_mod:
                    break
            except Exception as e:
                pass        
        
        if not py_mod:
            return None
        
        if hasattr(py_mod, name):
            ret = getattr(py_mod, name)
            return ret
            
        return None

    def getVersion(self):

        from wext_common.libcommon import formatVersion
    
        major = 0
        minor = 0
        build = 0
        patch = 0
    
        val = self.getDEF("DEF_PRODUCT_VERSION_MAJOR")
        if isinstance(val, int):
            major = val
        val = self.getDEF("DEF_PRODUCT_VERSION_MINOR")
        if isinstance(val, int):
            minor = val
        val = self.getDEF("DEF_VERSION_BUILD")
        if isinstance(val, int):
            build = val
        val = self.getDEF("DEF_VERSION_PATCH")
        if isinstance(val, int):
            patch = val
        
        return formatVersion(major, minor, build, patch)

    @staticmethod
    def init(app_name = "", rootPath = ""):
        Env.instance = Env(app_name, rootPath)
    
    def __init__(self, app_name, rootPath):

        import sys
        import os.path as p
        from os import environ as e
                
        self.appName = ""
        if app_name and len(app_name):
            e["APP_NAME"]=app_name           
            self.appName = app_name
        
        if e.get("WINGDB_ACTIVE"):
            if not e.get("DEBUG"):
                e["DEBUG"]="True"
        else:
            if self.getDEF("DEF_DEBUG"):
                os.putenv("DEBUG", True)

        self.inBundle = False
        if getattr( sys, 'frozen', False ):
            self.inBundle = True

        if not rootPath or \
           not len(rootPath) or \
           not p.exists(rootPath):
            self.rootPath = ""
            if self.inBundle:
                self.rootPath = sys._MEIPASS
            else:
                self.rootPath = p.dirname(__file__)
        else:
            self.rootPath = rootPath
        sys.path.append(self.rootPath)

        from wext_common.libcommon import trace_env, trace_env_init              

        if not len(app_name):
            if self.inBundle:
                self.appName = p.basename(sys.argv[0])
            else:
                self.appName = trace_env.app_name
                
        trace_env_init()



