#Lib: not executable
# -*- coding: UTF-8 -*-

from .systemd import SystemdNotifier
import time
import os
import logging

class NoneNtf(object):
    def notifyStarted(self):
        pass

    def notifyReloadStart(self):
        pass

    def notifyReloaded(self):
        pass

    def notifyStopping(self):
        pass

    def notifyMessage(self, msg):
        pass

    def notifyKeepAlive(self, force = False):
        pass
    
    def kaPeriodMS(self):
        return None
    
class SystemdNtf(NoneNtf):

    def __init__(self, name):
        self._impl = SystemdNotifier()
        if not self._impl.isValid():
            del self._impl
            self._impl = None
        else:
            self.log = logging.getLogger(name)
            self._kaPeriodMS = 2000
            val = os.getenv('WATCHDOG_USEC')
            if val:
                val = int(val) / 1000
                self._kaPeriodMS = val / 2
            self._lastKaTime = 0
            self.log.debug("Systemd notifier is ready. Watchdoc tick = %d ms", self._kaPeriodMS)

    def kaPeriodMS(self):
        if self._impl:
            return self._kaPeriodMS
        else:
            return None
    
    def notifyStarted(self):
        if self._impl:
            ntf = "READY=1"
            self.log.debug("Systemd would notified '%s'", ntf)
            self._impl.notify(ntf)

    def notifyReloadStart(self):
        if self._impl:
            ntf = "RELOADING=1"
            self.log.debug("Systemd would notified '%s'", ntf)
            self._impl.notify(ntf)

    def notifyReloaded(self):
        self.notifyStarted()

    def notifyStopping(self):
        if self._impl:
            ntf = "STOPPING=1"
            self.log.debug("Systemd would notified '%s'", ntf)
            self._impl.notify(ntf)

    def notifyMessage(self, msg):
        if self._impl:
            ntf = "STATUS=" + msg
            self.log.debug("Systemd would notified '%s'", ntf)
            self._impl.notify(ntf)

    def notifyKeepAlive(self, force = False):
        if self._impl:
            currTime = time.time()
            if not force and self._lastKaTime > 0:
                if currTime - self._lastKaTime < self._kaPeriodMS:
                    return
            self._lastKaTime = currTime
            ntf = "WATCHDOG=1"
            self.log.debug("Systemd would notified '%s'", ntf)
            self._impl.notify(ntf)

class ReviverNtf(NoneNtf):
    
    def __init__(self):
        #TODO
        
        raise NotImplemented
    