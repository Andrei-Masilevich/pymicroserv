'''
Required modules: pika
'''

import logging

from ..config import ConfRabbitMQ

from .db_node import DBNode 
from ..app import App

from tornado import gen

import time

class RabbitMQNode(DBNode, metaclass = ConfRabbitMQ):
    """Universal class for publishing and consuming
       in RabbitMQ broker
       with TornadoConnection connection.
    """
    
    def __init__(self, conf, name):
        
        super(RabbitMQNode, self).__init__(conf, name)
    
        from ..config import ConfGroup

        if not isinstance(conf, ConfGroup):
            raise TypeError("Config object must be group")
        
        val = conf.get("reconnect_timeout_ms")
        if not val:
            val = self.PING_TIMEOUT_MS #from DBNode.__init__
        elif not isinstance(val, int) or val < 10:
            raise ValueError("Invalid reconnect_timeout_ms value")
            
        setattr(self, "RECONNECT_TIMEOUT_MS", val) 

        self.__reset()
        
    def __reset(self):
        
        self._channel = None
        self._queue_name = None
        self._consumer_tag = None
        
        self._opening_future = None
        self._closing = False
        self._closing_future = None
            
        self._onMessageCallback = None
        self._onValidateMessageCallback = None

    @gen.coroutine
    def _createConnection(self, **callbacks):
        """Overloading abstract _createConnection protected method
        
        :param dict callbacks: The callbacks dictionary
        
        """
        
        self.log.info("Try to create topology")
            
        onMessageCallback = callbacks.get("onMessageCallback")
        if onMessageCallback:
            self._onMessageCallback = onMessageCallback
            
        onValidateMessageCallback = callbacks.get("onValidateMessageCallback")
        if onValidateMessageCallback:
            self._onValidateMessageCallback = onValidateMessageCallback
         
        import pika #declared here because pika creates loggers in headers, just when 'import pika' has called
    
        self.log.debug(
            "Connecting to '%s:%d'", 
            self.HOST, 
            self.PORT)
    
        credentials = pika.PlainCredentials(
            self.LOGIN, 
            self.PSW)
        params = pika.ConnectionParameters(
            self.HOST, 
            self.PORT, 
            self.VHOST, 
            credentials,
            heartbeat_interval = self.HEARTBEAT_S,
            frame_max = self.FRAME_MAX)
    
        self._opening_future = gen.Future()
        conn = pika.adapters.TornadoConnection(
            params, 
            self.__on_connection_open,
            self.__on_connection_error,
            stop_ioloop_on_close=False)
        result = yield self._opening_future
        if not result:
            raise gen.Return(None)

        self.log.debug(
            "Connection '%s:%d' opened",
            self.HOST, 
            self.PORT)
        
        self.log.debug("Adding connection close callback")
        conn.add_on_close_callback(self.__on_connection_closed)
       
        self.log.debug("Creating a new channel")
        self._opening_future = gen.Future()
        conn.channel(self.__on_channel_open)
        self._channel = yield self._opening_future
        if not self._channel:
            raise gen.Return(None)
        
        self.log.debug("Channel opened")

        self.log.debug("Adding channel close callback")
        self._channel.add_on_close_callback(self.__on_channel_closed)
    
        self.log.debug("Declaring exchange '%s'", 
                           self.EXCHANGE_NAME)
        self._opening_future = gen.Future()
        self._channel.exchange_declare(
            self.__on_exchange_declared,
            exchange = self.EXCHANGE_NAME,
            exchange_type = self.EXCHANGE_TYPE,
            passive = self.EXCHANGE_PASSIVE,
            durable = self.EXCHANGE_DURABLE,
            auto_delete = self.EXCHANGE_AUTO_DELETE)
        result = yield self._opening_future
        if not result:
            raise gen.Return(None)
        
        self.log.debug("Exchange '%s' declared", 
                           self.EXCHANGE_NAME)

        if not self.QUEUE_OFF:
        
            if self.QUEUE_MESSAGE_TTL_MS:
                arguments = {'x-message-ttl' : self.QUEUE_MESSAGE_TTL_MS}
            else:
                arguments = None
                
            self.log.debug("Declaring queue '%s'", 
                               self.QUEUE_NAME)
            self._opening_future = gen.Future()
            self._channel.queue_declare( 
                self.__on_queue_declared,
                queue = self.QUEUE_NAME,
                exclusive = self.QUEUE_EXCLUSIVE,
                passive = self.QUEUE_PASSIVE,
                durable = self.QUEUE_DURABLE,
                auto_delete = self.QUEUE_AUTO_DELETE,
                arguments = arguments)
            method_frame = yield self._opening_future
            if not method_frame:
                raise gen.Return(None)
            
            self._queue_name = method_frame.method.queue
            self.log.debug("Queue '%s' declared", 
                               self._queue_name)

            if self.QUEUE_BIND:
                self.log.debug("Binding queue '%s' to exchange '%s' via routing key '%s'", 
                                   self._queue_name,
                                   self.EXCHANGE_NAME,
                                   self.ROUTING_KEY)
                self._opening_future = gen.Future()
                self._channel.queue_bind(
                    self.__on_queue_binded,
                    self._queue_name,
                    self.EXCHANGE_NAME, 
                    self.ROUTING_KEY)
                result = yield self._opening_future
                if not result:
                    raise gen.Return(None)
            
                self.log.debug("Queue '%s' binded to exchange '%s' via routing key '%s'", 
                                   self._queue_name,
                                   self.EXCHANGE_NAME,
                                   self.ROUTING_KEY) 
        
            self.log.debug("Consuming queue '%s'", 
                               self._queue_name)
            
            self.log.debug("Adding consumer cancellation callback")
            self._channel.add_on_cancel_callback(self.__on_consumer_cancelled)
            
            if self.QOS_PREFETCH_COUNT > 0:
                self._channel.basic_qos(prefetch_count = self.QOS_PREFETCH_COUNT)
            self._consumer_tag = self._channel.basic_consume(self.__on_message,
                                                                 self._queue_name,
                                                                 no_ack = self.NO_ACK)
            self.log.debug("Queue '%s' consumed with tag '%s'", 
                               self._queue_name, 
                               self._consumer_tag)
        
        raise gen.Return(conn)
    
    def __on_connection_open(self, unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :type unused_connection: pika.SelectConnection

        """
        self.log.debug('Connection opened')
        if self._opening_future:
            self._opening_future.set_result(True)

    def __on_connection_error(self, unused_connection, text):
        self.log.error("Connection error '%r'", text)
        if self._opening_future:
            self._opening_future.set_result(False)

    def __on_channel_open(self, channel):
        self.log.debug('Channel opened')
        if self._opening_future:
            self._opening_future.set_result(channel)
    
    def __on_exchange_declared(self, unused_frame):
        self.log.debug('Exchange declared')
        if self._opening_future:
            self._opening_future.set_result(True)
        
    def __on_queue_declared(self, method_frame):
        self.log.debug('Queue declared')
        if self._opening_future:
            self._opening_future.set_result(method_frame)
    
    def __on_queue_binded(self, unused_frame):
        self.log.debug('Queue binded')
        if self._opening_future:
            self._opening_future.set_result(True)
        
    @gen.coroutine
    def _closeConnection(self, conn):
        """Overloading DBNode abstract _closeConnection protected method
    
        :param object conn: The connection object that is returned by _createConnection
    
        """        
        
        assert self._channel, "Channel object must exist in this call"
        
        self.log.debug("Stopping")
        self._closing = True
        
        if not self.QUEUE_OFF:
            self.log.debug("Sending a Basic.Cancel RPC command to RabbitMQ")
            self._closing_future = gen.Future()
            self._channel.basic_cancel(
                self.__on_consumer_cancel_ok,
                self._consumer_tag)
            result = yield self._closing_future
            if not result:
                raise gen.Return(False)
            self.log.debug("RabbitMQ acknowledged the cancellation of the consumer")
    
        self.log.debug("Closing the channel")
        self._closing_future = gen.Future()
        self._channel.close()
        result = yield self._closing_future
        if not result:
            raise gen.Return(False)
        
        self.__reset()
        
        raise gen.Return(True)

    def __on_consumer_cancel_ok(self, unused_frame):
        if self._closing_future:
            self._closing_future.set_result(True)
            
    def __on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """
        
        if not self._closing:
            self.log.warning("Consumer was cancelled remotely, shutting down: '%r'",
                                 method_frame)
            self.disconnect()

    def __on_channel_closed(self, channel, reply_code, reply_text):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel: The closed channel
        :param int reply_code: The numeric reason the channel was closed
        :param str reply_text: The text reason the channel was closed

        """
            
        if not self._closing:
            self.log.warning("Channel '%i' was closed: (%s) %s",
                                     channel, reply_code, reply_text)
            self.disconnect()
        else:
            self.log.debug("Closing connection")
            self.__connection().close()

    def __on_connection_closed(self, connection, reply_code, reply_text):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will _reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection connection: The closed connection obj
        :param int reply_code: The server provided reply_code if given
        :param str reply_text: The server provided reply_text if given

        """

        if not self._closing:
            self.log.warning("Connection closed, reopening in '%d' ms: (%s) %s",
                                     self.RECONNECT_TIMEOUT_MS, 
                                   reply_code, 
                                   reply_text)
            self.disconnect()
            
        if self._closing_future:
            self._closing_future.set_result(True)

    def _isConnected(self, conn):
        """Overloading DBNode abstract _isConnected protected method
    
        :param object conn: The connection object that is returned by _createConnection
    
        """
        
        return not conn.is_closed

    def __connection(self):
        """To hide public DBNode.connection method"""
        
        return super(RabbitMQNode, self).connection()
    
    def connection(self, connect = True):
        """This method must not to be used"""
        
        raise NotImplemented
    
    def ping(self, connect = True):
        """This method must not to be used"""
        
        raise NotImplemented
    
    def __on_message(self, unused_channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel unused_channel: The channel object
        :param pika.Spec.Basic.Deliver: basic_deliver method
        :param pika.Spec.BasicProperties: properties
        :param str|unicode body: The message body

        """
        
        try:
            if properties.content_type != self.CONTENT_TYPE:
                self.log.warning("Content Type '%s' is not equal expected '%s'", 
                                 properties.content_type, 
                                 self.CONTENT_TYPE)

            if self.SCHEMA_VALIDATION_ON and self._onValidateMessageCallback:
                bValid = self._onValidateMessageCallback(body, self.SCHEMA, self.CONTENT_TYPE)
            else:
                bValid = True

            if not bValid:
                self.log.error("Invalid message received '%s'", body)
            else:
                if self._onMessageCallback:
                    data = {
                        'routing_key': basic_deliver.routing_key, 
                        'content_type': properties.content_type, 
                        'basic_deliver': basic_deliver, 
                        'properties': properties, 
                        'body': body,
                        'correlation_id': properties.correlation_id,
                        'reply_to': properties.reply_to}
                    self._onMessageCallback(**data)
                    
            if not self.NO_ACK:
                self.__acknowledge_message(basic_deliver.delivery_tag)
        except Exception as err:
            self.log.warning("Exception '%s' while process message", err)
            raise
    
    def __acknowledge_message(self, delivery_tag):
        """Acknowledge the message delivery from RabbitMQ by sending a
        Basic.Ack RPC method for the delivery tag.

        :param int delivery_tag: The delivery tag from the Basic.Deliver frame

        """
        
        self.log.debug("Acknowledging message '%s'", delivery_tag)
        self._channel.basic_ack(delivery_tag)
    
    def changeCallbacks(self, **callbacks):

        super(RabbitMQNode, self).changeCallbacks(**callbacks)

        onMessageCallback = callbacks.get("onMessageCallback")
        if onMessageCallback:
            self._onMessageCallback = onMessageCallback
            
        onValidateMessageCallback = callbacks.get("onValidateMessageCallback")
        if onValidateMessageCallback:
            self._onValidateMessageCallback = onValidateMessageCallback

    def publishMessage(self, 
                       body,
                       connect = True,
                       content_type = None, 
                       exchange = None, 
                       routing_key = None,
                       correlation_id = None,
                       rpc_response = False):
        """Send message to Rabbit
    
        :param bool connect: To check and recreate connection if it is failed
        """
        
        if not connect and not self.isConnected():
            self.log.error("Node is not connected to publish message")
            return False 
        
        if rpc_response:
            _exchange = ''
        else:
            if exchange:
                _exchange = exchange
            else:
                _exchange = self.EXCHANGE_NAME

        if routing_key:
            _routing_key = routing_key
        else:
            _routing_key = self.ROUTING_KEY
            
        if self._queue_name and _routing_key == self._queue_name and self.QUEUE_DURABLE:
            _delivery_mode = 2
        else:
            _delivery_mode = 1
        
        import pika
        
        if self.MESSAGE_TTL_MS:
            timestamp = time.time()
            expire = str(self.MESSAGE_TTL_MS)
        else:
            timestamp = None
            expire = None

        props = {"content_type":  (content_type if content_type else self.CONTENT_TYPE),
                 "delivery_mode": _delivery_mode,
                 "timestamp": timestamp,
                 "expiration": expire}
        if not correlation_id is None:
            #correlation_id switch on RPC

            props["correlation_id"] = str(correlation_id)
            if not rpc_response:
                if not self._queue_name:
                    self.log.error("No queue to replay RPC request")
                    return False
                props["reply_to"] = self._queue_name

        _properties = pika.BasicProperties(**props)
        try:
            self._channel.basic_publish( exchange = _exchange, 
                                         routing_key = _routing_key, 
                                         properties = _properties,
                                         body = body)
        except Exception as err:
            self.log.error("Error '%s'", str(err))
            self.disconnect() #to recall connect next time
            return False         
        
        return True
    

