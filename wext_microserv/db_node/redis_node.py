'''
Required modules: redis, (hiredis)
'''

import logging

from ..config import ConfRedis

from .db_node import DBNode

class RedisNode(DBNode, metaclass = ConfRedis):
        
    def __init__(self, conf, name, master_mode = True):
        
        super(RedisNode, self).__init__(conf, name)
        self._master_mode = master_mode

    def _createConnection(self, **callbacks):
        self.log.info("Try connect to Redis")
        
        from redis import StrictRedis
        
        socket_timeout = self.SOCKET_TIMEOUT_MS / 1000.0
        
        _redis = None
        try:
            if len(self.SENTINELS):
                
                from redis.sentinel import Sentinel, SentinelConnectionPool
                
                sentinels = Sentinel(self.SENTINELS, 
                                     db = self.DBNUM, 
                                     password = self.PSW,
                                     socket_timeout = socket_timeout,
                                     max_connections = self.MAX_CONNECTIONS,
                                     retry_on_timeout = self.RETRY_ON_TIMEOUT)
                n = len(sentinels.sentinels)
                ci = 0
                sentinels_info = None
                while ci < n:
                    st = sentinels.sentinels[ci]
                    try:
                        sentinels_info = st.sentinel_masters()
                        break
                    except Exception as e:
                        self.log.warning('Failed to resolve sentinel "%s". Error: "%s"', str(self.SENTINELS[ci]), e)
                    ci += 1
                
                if sentinels_info:
                    self.log.debug("Sentinel info reveived: %s", str(sentinels_info))
                    
                service_name = sentinels_info.keys()[0]
                
                if self._master_mode:
                    _redis = sentinels.master_for(service_name)
                else:
                    _redis = sentinels.slave_for(service_name)
                    
            if not _redis:
                
                from redis import ConnectionPool
                
                connections_pool = ConnectionPool(host=self.HOST, 
                                                   port=self.PORT, 
                                                   db=self.DBNUM,
                                                   password = self.PSW,
                                                   socket_timeout = socket_timeout,
                                                   max_connections = self.MAX_CONNECTIONS,
                                                   retry_on_timeout = self.RETRY_ON_TIMEOUT)
                _redis = StrictRedis(connection_pool = connections_pool)
            
            _redis.ping()
            
            if self.MARK_CONNECTION:
                import uuid
                connection_name  = "%s-%s" % (self.name, uuid.uuid4())
                
                _redis.client_setname(connection_name)
            
            return _redis
            
        except Exception as e:
            self.log.error('Failed to connect. Error: "%s"', e)
        
        return None
        
    def _pingConnection(self, conn):
        
        self.connection().ping()
        