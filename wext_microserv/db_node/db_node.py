import logging

from tornado import gen
import tornado.ioloop

from ..patterns import reenterlock, reenterlockCoroutine

class DBNode(object):
    ''' Abstract DBNode
    '''

    def __init__(self, conf, name):

        def_val = 100
        if conf:
            val = conf.get("ping_timeout_ms")
            if not val:
                val = def_val
            elif not isinstance(val, int) or val < 100:
                raise ValueError("Invalid ping_timeout_ms value")
        else:
            val = def_val
        
        setattr(self, "PING_TIMEOUT_MS", val)
        
        self.conf = conf
        self.name = name
        self.log = logging.getLogger(type(self).__name__.lower())
        self._connection = None
        self._connection_callbacks = {}

    def changeCallbacks(self, **callbacks):
        self._connection_callbacks = callbacks

    @reenterlockCoroutine
    @gen.coroutine
    def connect(self, **callbacks):
        ret = False
        if self._connection:
            ret = True
        else:
            _con = None
            try:
                
                _con = yield self._createConnection(**callbacks)
                ret = (_con is not None)
                if ret:
                    self._connection = _con
                    self._connection_callbacks = callbacks
                
            except Exception as e:
                self.log.error('Failed to connect. Error: "%s"', e)

        raise gen.Return(ret)
    
    @reenterlockCoroutine
    @gen.coroutine
    def disconnect(self):
        ret = False
        if not self._connection:
            ret = True
        else:
            try:
                ret = yield self._closeConnection(self._connection)
                if ret:
                    del self._connection
                    self._connection = None
                    
            except Exception as e:
                self.log.error('Failed to disconnect. Error: "%s"', e)

        raise gen.Return(ret)

    @reenterlockCoroutine
    @gen.coroutine
    def ping(self):        
        if not self.isConnected():
            yield self.restoreConnection()
            
        ret = None
        try:
            ret = yield self._pingConnection(self._connection)
        except Exception as e:
            self.log.error('Failed to ping connection. Error: "%s"', e)
            yield self.restoreConnection()
    
        raise gen.Return(ret)
        
    def isConnected(self):
        if self._connection and \
           self._isConnected(self._connection):
            return True
        
        return False
    
    @gen.coroutine
    def invalidateConnection(self, func, *args,**kwargs):
        ret = None
        try:
            ret = func(*args,**kwargs)
        except Exception as e:
            self.log.error('Invalidate connection failed. Error: "%s"', e)
            yield self.ping()
        raise gen.Return(ret)
    
    def connection(self):
        
        if not self.isConnected():
            raise ValueError("Disconnected")
        
        return self._connection
    
    @reenterlockCoroutine
    @gen.coroutine
    def restoreConnection(self):
        if self._connection:
            try:
                yield self._closeConnection(self._connection)
                del self._connection
            except Exception as e:
                #not critical
                self.log.debug('Failed to disconnect. Error: "%s"', e)
                
            self._connection = None

        ret = yield self.connect(self._connection_callbacks)
        
        raise gen.Return(ret)

    @gen.coroutine
    def _createConnection(self, **callbacks):
        """overwrite"""
        yield None

    @gen.coroutine
    def _closeConnection(self, conn, **callbacks):
        """
        By default self.disconnect closes conn in destructor (__del__)
        If you don't want to destroy connection object, return False
        """
        yield False
    
    @gen.coroutine
    def _pingConnection(self, conn):
        """By default self.ping only checks connection state """
        yield False 
    
    def _isConnected(self, conn):
        """Additional state check (available) for conection """
        return True
    