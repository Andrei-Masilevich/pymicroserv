'''
Required modules: psycopg2
'''

import logging

from ..config import ConfPostgres

from contextlib import contextmanager

from .db_node import DBNode

class PostgresNode(DBNode, metaclass = ConfPostgres):
    
    def __init__(self, conf, name):
        
        super(PostgresNode, self).__init__(conf, name)
    
    @contextmanager
    def _getcursor(self):
       
        cons = self.connection()
        con = cons.getconn()
        try:
            yield con.cursor()
        finally:
            cons.putconn(con)
     
    def _call(self, callFunc):
        
        import psycopg2
    
        try:        
            with self._getcursor() as cur:
                callFunc(cur)
    
                while True:
                    ret = cur.fetchone()
                    if ret:
                        yield ret
                    else:
                        break
    
        except (Exception, psycopg2.DatabaseError) as error:
            self.log.error(error)        

    def _createConnection(self, **callbacks):
        from psycopg2.pool import SimpleConnectionPool
        import psycopg2
    
        self.log.debug('Connection pool will be created')
    
        dbConnection = "host='%s' port='%d' dbname='%s' user='%s' password='%s'"
        try:
            conn =  SimpleConnectionPool(self.MIN_CONNECTIONS,
                                                      self.MAX_CONNECTIONS,
                                                      dsn = dbConnection % (self.HOST,
                                                                            self.PORT,
                                                                            self.DBNAME,
                                                                            self.USER,
                                                                            self.PSW)
                                                      )
            self.log.info('Connection pool created')
            return conn
        except (Exception, psycopg2.DatabaseError) as error:
            self.log.error(error)
        
        return None
    
    def _closeConnection(self, conn):
        self.log.debug('Try close connections in pool')
        conn.closeall()
        return True
    
    def _pingConnection(self, conn):
        import psycopg2

        with self._getcursor() as cur:
            cur.execute("SELECT 1")
            (ret, ) = cur.fetchall()[0]
            if ret != 1:
                raise psycopg2.DatabaseError
    
    def get(self, query, vars=None, connect = True):
        '''generator for select queries'''
        
        if not query or not len(query):
            self.log.error('Empty query')
            return
        
        if not connect and not self.isConnected():
            return
                        
        self.log.debug("Try execute query '%s' with params %s", query, str(vars))
        
        lbCall = lambda cur, query = query, vars = vars: cur.execute(query, vars)
        for ret in self._call(lbCall):
            yield ret
    
    def callproc(self, procname, parameters=None, connect = True):
        '''generator to call DB procedures'''

        if not procname or not len(procname):
            self.log.error('Empty procname')
            return
        
        if not connect and not self.isConnected():
            return
        
        self.log.debug("Try execute procedure '%s' with params %s", procname, str(parameters))
        
        lbCall = lambda cur, procname = procname, parameters = parameters: cur.callproc(procname, parameters)
        for ret in self._call(lbCall):
            yield ret